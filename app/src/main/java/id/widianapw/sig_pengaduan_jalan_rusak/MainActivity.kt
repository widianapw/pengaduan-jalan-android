package id.widianapw.sig_pengaduan_jalan_rusak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.auth.LoginActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.auth.RegisterActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {
    private lateinit var preferences: Preferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = Preferences(this)
        button_login_main.setOnClickListener {
            startActivity<LoginActivity>()
        }
        button_register_main.setOnClickListener {
            startActivity<RegisterActivity>()
        }
    }
}

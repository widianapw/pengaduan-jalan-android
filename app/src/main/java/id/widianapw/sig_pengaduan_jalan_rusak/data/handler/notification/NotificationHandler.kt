package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.notification

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification.Notification
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.notification.NotificationContract
import io.reactivex.Observable

class NotificationHandler : BaseHandler() {
    private val service = getClient().create(NotificationContract.Handler::class.java)
    fun loadData(): Observable<BaseResultStandardData<List<Notification>>> {
        return service.loadData()
    }
}
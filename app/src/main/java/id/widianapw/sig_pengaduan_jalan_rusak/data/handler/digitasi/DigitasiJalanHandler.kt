package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.digitasi

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.digitasi.DigitasiJalan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.digitasi.DigitasiJalanContract
import io.reactivex.Observable

class DigitasiJalanHandler : BaseHandler() {
    val service = getClient().create(DigitasiJalanContract.Handler::class.java)
    fun loadData(): Observable<BaseResultStandardData<List<DigitasiJalan>>> {
        return service.loadData()
    }
}
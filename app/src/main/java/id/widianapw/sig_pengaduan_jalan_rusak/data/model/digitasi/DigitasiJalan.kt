package id.widianapw.sig_pengaduan_jalan_rusak.data.model.digitasi

import com.google.gson.annotations.Expose

data class DigitasiJalan(
    @Expose
    var id: String = "",
    @Expose
    var namaJalan: String = "",
    @Expose
    var ruasJalan: String = "",
    @Expose
    var koordinatDigitasi: List<KoordinatDigitasi>? = null
)


data class KoordinatDigitasi(
    @Expose
    var latitude: Double = 0.0,
    @Expose
    var longitude: Double = 0.0
)
package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

class BaseResultStandardData<T> : BaseResultData() {
    var data: T? = null
}
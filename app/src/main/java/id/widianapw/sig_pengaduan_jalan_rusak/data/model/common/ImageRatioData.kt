package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

data class ImageRatioData(
    var x: Int, var y: Int
)


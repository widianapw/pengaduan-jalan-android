package id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan

import com.google.gson.annotations.Expose

data class Pengaduan(
    @Expose
    var id: String = "",
    @Expose
    var judul: String = "",
    @Expose
    var deskripsi: String = "",
    @Expose
    var status: String = "",
    @Expose
    var latitude: Double? = null,
    @Expose
    var longitude: Double? = null,
    @Expose
    var tanggal: String? = null,
    @Expose
    var username: String? = null,
    @Expose
    var userPhotoUrl: String? = null,
    @Expose
    var pengaduanPhotoUrl: String? = null,
    @Expose
    var namaJalan: String? = null,
    @Expose
    var jenisJalan: String? = null,
    @Expose
    var pengelolaJalan: String? = null,
    @Expose
    var alamat: String? = null,
    @Expose
    var images: List<PengaduanImage>? = null
)

data class PengaduanImage(
    var type: String,
    var image: String
)
package id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification

import com.google.gson.annotations.Expose

data class Notification(
    @Expose
    var judul: String = "",
    @Expose
    var deskripsi: String = "",
    @Expose
    var tanggal: String = ""
)
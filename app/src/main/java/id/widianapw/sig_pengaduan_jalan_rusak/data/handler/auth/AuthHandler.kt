package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.auth

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth.AuthContract
import io.reactivex.Observable

class AuthHandler : BaseHandler() {
    private val service = getClient().create(AuthContract.Handler::class.java)
    fun login(data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>> {
        return service.login(data)
    }

    fun register(data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>> {
        return service.register(data)
    }
}
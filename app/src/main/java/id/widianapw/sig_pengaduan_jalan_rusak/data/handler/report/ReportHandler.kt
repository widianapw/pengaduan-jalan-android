package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.report

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.report.ReportContract
import io.reactivex.Observable
import okhttp3.MultipartBody

class ReportHandler : BaseHandler() {
    val service = getClient().create(ReportContract.Handler::class.java)
    fun report(data: HashMap<String, Any?>): Observable<BaseResultStandardData<Pengaduan>> {
        return service.lapor(data)
    }

    fun uploadImagePengaduan(
        data: MultipartBody.Part,
        id: String
    ): Observable<BaseResultStandardData<Pengaduan>> {
        return service.uploadImagePengaduan(data, id)
    }

    fun uploadMultipleImagePengaduan(
        data: ArrayList<MultipartBody.Part>,
        id: String
    ): Observable<BaseResultStandardData<Pengaduan>> {
        return service.uploadMultipleImagePengaduan(data, id)
    }
}
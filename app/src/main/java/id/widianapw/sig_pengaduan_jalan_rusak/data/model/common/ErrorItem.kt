package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

import com.google.gson.annotations.Expose

class ErrorItem {
    @Expose
    val title : String?=null

    @Expose
    val message : String?=null
}
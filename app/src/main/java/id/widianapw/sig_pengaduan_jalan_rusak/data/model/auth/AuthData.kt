package id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth

import com.google.gson.annotations.Expose
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultData

data class AuthData(
    @Expose
    var fcmToken: String? = null,
    @Expose
    var accessToken: String? = null,
    @Expose
    var email: String? = null,
    @Expose
    var username: String? = null,
    @Expose
    var photoUrl: String? = null,
    @Expose
    var phone: String? = null
) : BaseResultData()
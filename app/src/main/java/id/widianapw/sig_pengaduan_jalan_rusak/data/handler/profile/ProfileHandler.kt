package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.profile

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileEditContract
import io.reactivex.Observable
import okhttp3.MultipartBody

class ProfileHandler : BaseHandler() {
    private val service = getClient().create(ProfileContract.Handler::class.java)
    private val editService = getClient().create(ProfileEditContract.Handler::class.java)
    fun loadData(): Observable<BaseResultStandardData<ProfileDisplayData>> {
        return service.loadData()
    }

    fun updateProfile(data: HashMap<String, Any?>): Observable<BaseResultStandardData<ProfileDisplayData>> {
        return editService.updateProfile(data)
    }

    fun updateProfileImage(part: MultipartBody.Part): Observable<BaseResultStandardData<ProfileDisplayData>> {
        return editService.updateProfileImage(part)
    }
}
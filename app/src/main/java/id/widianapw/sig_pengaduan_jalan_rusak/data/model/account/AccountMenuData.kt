package id.widianapw.sig_pengaduan_jalan_rusak.data.model.account

import android.content.Context
import android.graphics.drawable.Drawable
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Utilities


data class AccountMenuData(
    val id: Int = 0,
    val name: String? = null,
    val img: Drawable? = null
)

fun getAccountMenuList(context: Context): List<AccountMenuData> {
    val listMenu = arrayListOf<AccountMenuData>()
    val itemData = Utilities.getStringArray(context.resources.getStringArray(R.array.account_menu))
    val menuDrawable = arrayListOf<Drawable>(
        context.resources.getDrawable(R.drawable.portfolio),
        context.resources.getDrawable(R.drawable.telephone)
    )
    itemData.forEachIndexed { index, (id, title) ->
        listMenu += AccountMenuData(id, title, menuDrawable[index])
    }

    return listMenu
}

data class CallerList(val id: Int, val drawable: Drawable, val phone: String)

fun getCallerList(context: Context): List<CallerList> {
    val list = arrayListOf<CallerList>()

    list += CallerList(
        1,
        context.resources.getDrawable(R.drawable.viber),
        ""
    )

    list += CallerList(
        2,
        context.resources.getDrawable(R.drawable.whatsapp),
        ""
    )

    list += CallerList(
        3,
        context.resources.getDrawable(R.drawable.telegram),
        ""
    )

    return list
}

package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

import com.google.gson.annotations.Expose

/**
 * Created by ooyama on 2017/05/29.
 */

class ErrorData {
    @Expose
    val code: Int = 0
    @Expose
    val title: String? = null
    @Expose
    val errors: List<ErrorItem>? = null
}


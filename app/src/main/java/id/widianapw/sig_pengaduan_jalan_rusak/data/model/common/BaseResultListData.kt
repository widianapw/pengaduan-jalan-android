package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

class BaseResultListData<T> : BaseResultData() {
    var data: MutableList<T> = mutableListOf()
}
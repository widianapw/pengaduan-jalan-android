package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common

import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.FirebaseContract
import io.reactivex.Observable
import retrofit2.create

class FirebaseHandler: BaseHandler() {
    private val service = getClient().create(FirebaseContract.Handler::class.java)
    fun updateFirebaseToken(data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>>{
        return  service.updateFirebaseToken(data)
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.data.model.common

import com.google.gson.annotations.Expose

open class BaseResultData {
    @Expose
    var meta: Any? = null

    @Expose
    var link: Any? = null

}

open class APIError {
    val error: ErrorData = ErrorData()
}


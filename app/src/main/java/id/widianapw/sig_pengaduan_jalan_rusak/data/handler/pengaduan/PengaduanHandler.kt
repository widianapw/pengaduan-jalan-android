package id.widianapw.sig_pengaduan_jalan_rusak.data.handler.pengaduan

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanDetailContract
import io.reactivex.Observable

class PengaduanHandler: BaseHandler() {
    private val service = getClient().create(PengaduanContract.Handler::class.java)
    private val detailService= getClient().create(PengaduanDetailContract.Handler::class.java)
    fun loadData(): Observable<BaseResultStandardData<List<Pengaduan>>>{
        return service.loadData()
    }

    fun loadDataTerverifikasi(): Observable<BaseResultStandardData<List<Pengaduan>>>{
        return service.loadDataTerverifikasi()
    }

    fun loadPengaduanDetail(id: String): Observable<BaseResultStandardData<Pengaduan>>{
        return detailService.loadPengaduanDetail(id)
    }

    fun loadDataSearch(key: String): Observable<BaseResultStandardData<List<Pengaduan>>>{
        return service.loadDataSearch(key)
    }
}
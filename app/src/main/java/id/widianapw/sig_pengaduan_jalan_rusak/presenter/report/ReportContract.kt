package id.widianapw.sig_pengaduan_jalan_rusak.presenter.report

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface ReportContract {
    interface View : BaseContract {
        fun laporResponse(response: Pengaduan)
        fun uploadImageResponse(response: Pengaduan)
    }

    interface Presenter {
        fun lapor(data: HashMap<String, Any?>)
        fun uploadImagePengaduan(data: MultipartBody.Part, id: String)
        fun uploadMultiplaeImagePengaduan(data: ArrayList<MultipartBody.Part>, id: String)
    }

    interface Handler {
        @FormUrlEncoded
        @POST(Constants.APICtrl.postPengaduan)
        fun lapor(@FieldMap data: HashMap<String, Any?>): Observable<BaseResultStandardData<Pengaduan>>

        @Multipart
        @POST(Constants.APICtrl.uploadImagePengaduan)
        fun uploadImagePengaduan(
            @Part data: MultipartBody.Part,
            @Path(value = "id", encoded = true) id: String
        ): Observable<BaseResultStandardData<Pengaduan>>

        @Multipart
        @POST(Constants.APICtrl.uploadImagePengaduan)
        fun uploadMultipleImagePengaduan(
            @Part data: ArrayList<MultipartBody.Part>,
            @Path(value = "id", encoded = true) id: String
        ): Observable<BaseResultStandardData<Pengaduan>>
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.profile.ProfileHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe
import okhttp3.MultipartBody

class ProfileEditPresenter(val view: ProfileEditContract.View) : ProfileEditContract.Presenter {
    val handler = ProfileHandler()
    override fun updateData(data: HashMap<String, Any?>) {
        handler.updateProfile(data)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<ProfileDisplayData>>(view) {
                override fun onNext(t: BaseResultStandardData<ProfileDisplayData>) {
                    t.data?.let { view.updateProfileResponse(it) }
                }
            })
    }

    override fun updateProfileImage(part: MultipartBody.Part) {
        handler.updateProfileImage(part)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<ProfileDisplayData>>(view) {
                override fun onNext(t: BaseResultStandardData<ProfileDisplayData>) {
                    t.data?.let { view.updateProfileResponse(it) }
                }
            })
    }


}
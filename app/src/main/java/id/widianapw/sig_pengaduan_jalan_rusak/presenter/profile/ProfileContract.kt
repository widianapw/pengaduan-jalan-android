package id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ProfileContract {
    interface View : BaseContract {
        fun profileResponse(response: ProfileDisplayData)
    }

    interface Presenter {
        fun loadData()
    }

    interface Handler {
        @GET(Constants.APICtrl.getProfile)
        fun loadData(): Observable<BaseResultStandardData<ProfileDisplayData>>
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.pengaduan.PengaduanHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class PengaduanDetailPresenter(val view: PengaduanDetailContract.View) :
    PengaduanDetailContract.Presenter {
    val handler = PengaduanHandler()
    override fun loadPengaduanDetail(id: String) {
        handler.loadPengaduanDetail(id)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<Pengaduan>>(view) {
                override fun onNext(t: BaseResultStandardData<Pengaduan>) {
                    t.data?.let { view.pengaduanDetailResponse(it) }
                }
            })
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.presenter.common

interface BaseContract {
    fun showLoading(tag: String?=null)
    fun hideLoading(tag: String?=null)
    fun showError(title: String, message: String)
}
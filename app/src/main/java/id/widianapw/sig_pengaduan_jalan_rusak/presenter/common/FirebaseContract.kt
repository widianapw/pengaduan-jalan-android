package id.widianapw.sig_pengaduan_jalan_rusak.presenter.common

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import io.reactivex.Observable
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.*
import kotlin.collections.HashMap

interface FirebaseContract {
    interface View: BaseContract{
        fun updateFirebaseTokenResponse(response: AuthData)
    }

    interface Presenter{
        fun updateFirebaseToken(data: HashMap<String, Any?>)
    }

    interface Handler{
        @FormUrlEncoded
        @POST(Constants.APICtrl.updateFcmToken)
        fun updateFirebaseToken(@FieldMap data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>>
    }
}
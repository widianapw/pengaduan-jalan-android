package id.widianapw.sig_pengaduan_jalan_rusak.presenter.common

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.FirebaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class FirebasePresenter(val view: FirebaseContract.View) : FirebaseContract.Presenter {
    val handler = FirebaseHandler()
    override fun updateFirebaseToken(data: HashMap<String, Any?>) {
        handler.updateFirebaseToken(data)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<AuthData>>(view) {
                override fun onNext(t: BaseResultStandardData<AuthData>) {
                    t.data?.let { view.updateFirebaseTokenResponse(it) }
                }
            })
    }
}
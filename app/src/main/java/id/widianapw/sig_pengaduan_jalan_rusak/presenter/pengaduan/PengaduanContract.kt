package id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface PengaduanContract {
    interface View : BaseContract {
        fun pengaduanResponse(response: List<Pengaduan>)
    }

    interface Presenter {
        fun loadData()
        fun loadDataTerverifikasi()
        fun loadDataSearch(key: String)
    }

    interface Handler {
        @GET(Constants.APICtrl.getPengaduan)
        fun loadData(): Observable<BaseResultStandardData<List<Pengaduan>>>

        @GET(Constants.APICtrl.getPengaduanTerverifikasi)
        fun loadDataTerverifikasi(): Observable<BaseResultStandardData<List<Pengaduan>>>

        @GET(Constants.APICtrl.getPengaduanSearch)
        fun loadDataSearch(@Path("key") key: String): Observable<BaseResultStandardData<List<Pengaduan>>>
    }
}
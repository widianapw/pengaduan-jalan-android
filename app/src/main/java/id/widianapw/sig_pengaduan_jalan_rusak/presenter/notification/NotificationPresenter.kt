package id.widianapw.sig_pengaduan_jalan_rusak.presenter.notification

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.notification.NotificationHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification.Notification
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class NotificationPresenter(val view: NotificationContract.View) : NotificationContract.Presenter {
    val handler = NotificationHandler()
    override fun loadData() {
        handler.loadData()
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<List<Notification>>>(view) {
                override fun onNext(t: BaseResultStandardData<List<Notification>>) {
                    t.data?.let { view.notificationResponse(it) }
                }
            })
    }
}
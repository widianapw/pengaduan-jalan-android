package id.widianapw.sig_pengaduan_jalan_rusak.presenter.digitasi

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.digitasi.DigitasiJalan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET

interface DigitasiJalanContract {
    interface View : BaseContract {
        fun loadResponse(response: List<DigitasiJalan>)
    }

    interface Presenter {
        fun loadData()
    }

    interface Handler {
        @GET(Constants.APICtrl.getDigitasi)
        fun loadData(): Observable<BaseResultStandardData<List<DigitasiJalan>>>
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.presenter.digitasi

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.digitasi.DigitasiJalanHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.digitasi.DigitasiJalan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class DigitasiJalanPresenter(val view: DigitasiJalanContract.View) :
    DigitasiJalanContract.Presenter {
    private val handler = DigitasiJalanHandler()
    override fun loadData() {
        handler.loadData()
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<List<DigitasiJalan>>>(view) {
                override fun onNext(t: BaseResultStandardData<List<DigitasiJalan>>) {
                    t.data?.let { view.loadResponse(it) }
                }
            })
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.presenter.notification

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification.Notification
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET

interface NotificationContract {
    interface View : BaseContract {
        fun notificationResponse(response: List<Notification>)
    }

    interface Presenter {
        fun loadData()
    }

    interface Handler {
        @GET(Constants.APICtrl.getNotification)
        fun loadData(): Observable<BaseResultStandardData<List<Notification>>>
    }
}
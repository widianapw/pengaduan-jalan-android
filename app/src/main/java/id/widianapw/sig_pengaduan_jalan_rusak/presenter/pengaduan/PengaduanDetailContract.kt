package id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface PengaduanDetailContract {
    interface View : BaseContract {
        fun pengaduanDetailResponse(response: Pengaduan)
    }

    interface Presenter {
        fun loadPengaduanDetail(id: String)
    }

    interface Handler {
        @GET(Constants.APICtrl.getPengaduanDetail)
        fun loadPengaduanDetail(@Path("id") id: String): Observable<BaseResultStandardData<Pengaduan>>
    }
}
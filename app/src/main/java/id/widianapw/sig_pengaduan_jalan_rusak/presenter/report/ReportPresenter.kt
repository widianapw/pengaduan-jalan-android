package id.widianapw.sig_pengaduan_jalan_rusak.presenter.report

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.report.ReportHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe
import okhttp3.MultipartBody

class ReportPresenter(val view: ReportContract.View) : ReportContract.Presenter {
    val handler = ReportHandler()
    override fun lapor(data: HashMap<String, Any?>) {
        handler.report(data)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<Pengaduan>>(view) {
                override fun onNext(t: BaseResultStandardData<Pengaduan>) {
                    t.data?.let { view.laporResponse(it) }
                }
            })
    }

    override fun uploadImagePengaduan(data: MultipartBody.Part, id: String) {
        handler.uploadImagePengaduan(data, id)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<Pengaduan>>(view) {
                override fun onNext(t: BaseResultStandardData<Pengaduan>) {
                    t.data?.let { view.uploadImageResponse(it) }
                }
            })
    }

    override fun uploadMultiplaeImagePengaduan(data: ArrayList<MultipartBody.Part>, id: String) {
        handler.uploadMultipleImagePengaduan(data, id)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<Pengaduan>>(view) {
                override fun onNext(t: BaseResultStandardData<Pengaduan>) {
                    t.data?.let { view.uploadImageResponse(it) }
                }
            })
    }
}
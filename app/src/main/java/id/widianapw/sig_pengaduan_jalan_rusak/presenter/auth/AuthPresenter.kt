package id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.auth.AuthHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AuthPresenter(val view: AuthContract.View) : AuthContract.Presenter {
    private val authHandler = AuthHandler()
    override fun login(data: HashMap<String, Any?>) {
        authHandler.login(data)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<AuthData>>(view) {
                override fun onNext(t: BaseResultStandardData<AuthData>) {
                    t.data?.let { view.authResponse(it) }
                }
            })
    }

    override fun register(data: HashMap<String, Any?>) {
        authHandler.register(data)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ErrorHandler<BaseResultStandardData<AuthData>>(view) {
                override fun onNext(t: BaseResultStandardData<AuthData>) {
                    t.data?.let { view.authResponse(it) }
                }
            })
    }
}
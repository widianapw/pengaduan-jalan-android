package id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.pengaduan.PengaduanHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class PengaduanPresenter(val view: PengaduanContract.View) : PengaduanContract.Presenter {
    val handler = PengaduanHandler()
    override fun loadData() {
        handler.loadData()
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<List<Pengaduan>>>(view) {
                override fun onNext(t: BaseResultStandardData<List<Pengaduan>>) {
                    t.data?.let { view.pengaduanResponse(it) }
                }
            })
    }

    override fun loadDataTerverifikasi() {
        handler.loadDataTerverifikasi()
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<List<Pengaduan>>>(view) {
                override fun onNext(t: BaseResultStandardData<List<Pengaduan>>) {
                    t.data?.let { view.pengaduanResponse(it) }
                }

            })
    }

    override fun loadDataSearch(key: String) {
        handler.loadDataSearch(key)
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<List<Pengaduan>>>(view) {
                override fun onNext(t: BaseResultStandardData<List<Pengaduan>>) {
                    t.data?.let { view.pengaduanResponse(it) }
                }
            })
    }


}
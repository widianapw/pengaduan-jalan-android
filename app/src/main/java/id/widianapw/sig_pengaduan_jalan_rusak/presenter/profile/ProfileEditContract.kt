package id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface ProfileEditContract {
    interface View : BaseContract {
        fun updateProfileResponse(response: ProfileDisplayData)
    }

    interface Presenter {
        fun updateData(data: HashMap<String, Any?>)
        fun updateProfileImage(data: MultipartBody.Part)
    }

    interface Handler {
        @FormUrlEncoded
        @POST(Constants.APICtrl.updateProfile)
        fun updateProfile(@FieldMap data: HashMap<String, Any?>): Observable<BaseResultStandardData<ProfileDisplayData>>

        @Multipart
        @POST(Constants.APICtrl.updateProfileImage)
        fun updateProfileImage(@Part part: MultipartBody.Part): Observable<BaseResultStandardData<ProfileDisplayData>>
    }
}
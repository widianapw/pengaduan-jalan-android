package id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile

import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.ErrorHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.profile.ProfileHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.doSubscribe

class ProfilePresenter(val view: ProfileContract.View) :
    ProfileContract.Presenter {
    private val handler = ProfileHandler()
    override fun loadData() {
        handler.loadData()
            .doSubscribe(object : ErrorHandler<BaseResultStandardData<ProfileDisplayData>>(view) {
                override fun onNext(t: BaseResultStandardData<ProfileDisplayData>) {
                    t.data?.let { view.profileResponse(it) }
                }
            })
    }

}
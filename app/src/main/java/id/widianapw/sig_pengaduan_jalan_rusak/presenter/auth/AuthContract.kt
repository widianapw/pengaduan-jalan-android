package id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth

import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.BaseResultStandardData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.BaseContract
import io.reactivex.Observable
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthContract {
    interface View : BaseContract {
        fun authResponse(response: AuthData)
    }

    interface Presenter {
        fun login(data: HashMap<String, Any?>)
        fun register(data: HashMap<String, Any?>)
    }

    interface Handler {
        @FormUrlEncoded
        @POST(Constants.APICtrl.login)
        fun login(@FieldMap data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>>

        @FormUrlEncoded
        @POST(Constants.APICtrl.register)
        fun register(@FieldMap data: HashMap<String, Any?>): Observable<BaseResultStandardData<AuthData>>
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants.Permissions.Companion.AUTO_COMPLETE_REQUEST
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants.Permissions.Companion.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.BaseActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.map.MapDetailBottomSheet
import kotlinx.android.synthetic.main.fragment_maps.*

class MapsFragment : Fragment(), PengaduanContract.View {
    var map: GoogleMap? = null
    var mContext: Context? = null
    var baseActivity: BaseActivity? = null
    var locationPermission = false
    var marker: Marker? = null
    val presenter = PengaduanPresenter(this)
    var pengaduanList = mutableListOf<Pengaduan>()
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
//        val sydney = LatLng(-8.6756562, 115.2236804)
//        googleMap.addMarker(
//            MarkerOptions().position(sydney).title("Marker in Stikom").icon(
//                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
//            )
//        )
        getLocationPermission()
        try {
            getDeviceLocation()
            map?.isMyLocationEnabled = true

        } catch (e: SecurityException) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity
        presenter.loadDataTerverifikasi()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
        searchMap?.setOnClickListener {
            val intent = mContext?.let { it1 ->
                Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                    .setCountry("IDN")
                    .build(
                        it1
                    )
            }
            startActivityForResult(intent, AUTO_COMPLETE_REQUEST)
        }
    }

    fun loadData(){
        showLoading()
        presenter.loadDataTerverifikasi()
    }

    private fun getDeviceLocation() {
        val mLocation = mContext?.let { LocationServices.getFusedLocationProviderClient(it) }
        try {
            if (locationPermission) {
                val location = mLocation?.lastLocation
                location?.addOnCompleteListener {
                    val currentLocation = it.result
                    currentLocation?.latitude?.let { it1 -> LatLng(it1, currentLocation.longitude) }
                        ?.let { it2 ->
                            moveCamera(
                                it2, 18f
                            )
                        }

                }
            }

        } catch (e: SecurityException) {

        }
    }

    private fun moveCamera(latLng: LatLng, zoom: Float = 18f) {
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }


    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                GlobalClass.context,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermission = true
        } else {
            baseActivity?.let {
                ActivityCompat.requestPermissions(
                    it, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                locationPermission = grantResults.isNotEmpty() &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED

            }
        }
        updateLocationUI()
    }

    private fun updateLocationUI() {
        try {
            if (locationPermission) {
                map?.isMyLocationEnabled = true
                map?.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                getLocationPermission()
            }
        } catch (e: SecurityException) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AUTO_COMPLETE_REQUEST) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.let {
                        val place = Autocomplete.getPlaceFromIntent(data)
                        place.latLng?.let { it1 -> moveCamera(it1, 15f) }
//                        marker?.let { it1 -> removeMarker(it1) }
//                        marker = map?.addMarker(
//                            place.latLng?.let { it1 ->
//                                MarkerOptions().position(it1).title(place.address).icon(
//                                    BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
//                                )
//                            }
//                        )
                        Log.i("PLACE", "Place: ${place.name}, ${place.id}")
                    }
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    // TODO: Handle the error.
                    data?.let {
                        val status = Autocomplete.getStatusFromIntent(data)
                        Log.i("PLACE", status.statusMessage)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
            return
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    fun removeMarker(marker: Marker) {
        marker.remove()
    }

    override fun pengaduanResponse(response: List<Pengaduan>) {
        hideLoading()
        pengaduanList = response.toMutableList()
        for (i in response) {
            map?.apply {
                val latLng = i.latitude?.let { i.longitude?.let { it1 -> LatLng(it, it1) } }
                addMarker(
                    latLng?.let {
                        MarkerOptions()
                            .position(it)
                            .title(i.id)
                            .icon(
                                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
                            )
                    }
                )
            }
            map?.setOnMarkerClickListener { p0 ->
                showBottomSheet(p0.title)
                true
            }
        }
    }

    private fun showBottomSheet(data: String) {
        val pengaduan = pengaduanList.find {
            data == it.id
        }
        val bottomSheet = pengaduan?.let { MapDetailBottomSheet(it) }
        bottomSheet?.show(childFragmentManager, Constants.Tags.BOTTOM_SHEET_TAG)
    }

    override fun showLoading(tag: String?) {
//        baseActivity?.showProgressBar()
    }

    override fun hideLoading(tag: String?) {
//        baseActivity?.hideProgressBar()
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        baseActivity?.showErrorCustomDialog(title, message)
    }

}
package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.AccountMenuData
import kotlinx.android.synthetic.main.accountfragment_rv_content.view.*

class AccountMenuAdapter(val clickListener: (Int) -> Unit) :
    RecyclerView.Adapter<AccountMenuAdapter.ViewHolder>() {
    var listMenu: List<AccountMenuData>? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.accountfragment_rv_content, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = listMenu?.get(position)
        holder.itemView.apply {
            accountfragment_rv_img?.setImageDrawable(item?.img)
            accountfragment_rv_name?.text = item?.name
            if (position == listMenu?.size?.minus(1) ?: 0) accountfragment_rv_divider?.visibility =
                View.GONE
            accountfragment_layout?.setOnClickListener {
                item?.id?.let { it1 -> clickListener(it1) }
            }
        }
        holder.apply {
            setIsRecyclable(false)
        }
    }

    override fun getItemCount(): Int {
        return listMenu?.count() ?: 0
    }
}
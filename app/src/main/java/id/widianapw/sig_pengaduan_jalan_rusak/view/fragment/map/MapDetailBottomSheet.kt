package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.load
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.PengaduanImage
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.pengaduan.PengaduanDetailActivity
import kotlinx.android.synthetic.main.bottom_sheet_map.*
import org.jetbrains.anko.support.v4.startActivity

class MapDetailBottomSheet(val pengaduan: Pengaduan) : BottomSheetDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_BottomSheetDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottom_sheet_map, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        text_desc_pengaduan_map?.text = pengaduan.deskripsi
        text_judul_pengaduan_map?.text = pengaduan.judul
        text_pengelola_jalan_map?.text = pengaduan.pengelolaJalan
        val images = pengaduan.images?.filter { it.type == "pengaduan" }?.toMutableList()
        if (images.isNullOrEmpty())
            images?.add(PengaduanImage("pengaduan", pengaduan.pengaduanPhotoUrl ?: ""))

        carousel_pengaduan_map?.apply {
            setImageListener { position, imageView ->
                imageView.load(images?.get(position)?.image)
            }
            pageCount = images?.count() ?: 0
        }

        btn_lihat_detail_map?.setOnClickListener {
            startActivity<PengaduanDetailActivity>(
                Constants.Tags.ARGS_ID to pengaduan.id
            )
        }
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.report

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.extension.clickWithDebounce
import kotlinx.android.synthetic.main.item_report_image.view.*
import java.io.File

class ReportImageAdapter : RecyclerView.Adapter<ReportImageAdapter.ViewHolder>() {
    private var imageList = mutableListOf<File>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_report_image, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageFile = imageList[position]
        val uri = Uri.fromFile(imageFile)
        holder.itemView.apply {
            iv_report_image?.load(uri)
            btn_delete_report_image?.clickWithDebounce {
                removeData(imageFile)
            }
        }
    }

    override fun getItemCount(): Int {
        return imageList.count()
    }

    fun setData(data: List<File>) {
        imageList = data.toMutableList()
        notifyDataSetChanged()
    }

    fun addData(file: File) {
        imageList.add(file)
        notifyDataSetChanged()
    }

    fun removeData(file: File) {
        imageList.remove(file)
        notifyDataSetChanged()
    }

    fun getData(): MutableList<File> {
        return imageList
    }


}
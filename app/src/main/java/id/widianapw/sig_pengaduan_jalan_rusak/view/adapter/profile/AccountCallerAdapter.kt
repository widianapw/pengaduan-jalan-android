package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.CallerList
import kotlinx.android.synthetic.main.contactusbs_rv_content.view.*

class AccountCallerAdapter(
    val callerList: List<CallerList>,
    val clickListener: (Int) -> Unit

) : RecyclerView.Adapter<AccountCallerAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.contactusbs_rv_content, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = callerList[position]
        holder.apply {
            itemView.apply {
                contactusbs_rv_image.setImageDrawable(item.drawable)
                setOnClickListener {
                    clickListener(adapterPosition + 1)
                }
            }
            setIsRecyclable(false)

        }
    }

    override fun getItemCount(): Int {
        return callerList.count()
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.pengaduan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Utilities
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.setStatusColor
import id.widianapw.sig_pengaduan_jalan_rusak.extension.switchDateFormatString
import kotlinx.android.synthetic.main.item_pengaduan.view.*

class PengaduanAdapter(val clickListener: (String) -> Unit) :
    RecyclerView.Adapter<PengaduanAdapter.ViewHolder>() {
    var pengaduanList: List<Pengaduan>? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pengaduan, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = pengaduanList?.get(position)
        holder.itemView.apply {
            val tanggal =
                item?.tanggal?.switchDateFormatString(
                    Constants.Common.DATE_FORMAT_API,
                    Constants.Common.DATE_FORMAT_READ
                )
            val statusArray =
                Utilities.getStringArray(context.resources.getStringArray(R.array.pengaduan_status))

            cv_status_pengaduan?.setStatusColor(item?.status ?: "0")
            item?.status?.toInt()?.let { statusArray[it].title }

            iv_user_pengaduan?.load(item?.userPhotoUrl)
            text_username_pengaduan?.text = item?.username
            text_date_pengaduan?.text = tanggal
            text_id_pengaduan?.text = context.getString(R.string.id_pengaduan, item?.id)
            text_desc_pengaduan?.text = item?.deskripsi
            text_title_pengaduan?.text = item?.judul
            text_status_pengaduan?.text = item?.status?.toInt()?.let { statusArray[it].title }
            tv_instansi_pengaduan?.text = item?.pengelolaJalan
            setOnClickListener {
                item?.id?.let { it1 -> clickListener(it1) }
            }
        }
    }

    override fun getItemCount(): Int {
        return pengaduanList?.size ?: 0
    }

    fun setData(data: List<Pengaduan>) {
        pengaduanList = data
        notifyDataSetChanged()
    }
}
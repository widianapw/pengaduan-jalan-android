package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.profile

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import id.widianapw.sig_pengaduan_jalan_rusak.MainActivity
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass.Companion.applicationContext
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.AccountMenuData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.getAccountMenuList
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.getCallerList
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.gone
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfilePresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.SimpleDialogListener
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.profile.ProfileEditActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.profile.AccountMenuAdapter
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.support.v4.startActivity

class ProfileFragment : Fragment(), ProfileContract.View {
    private var mContext: Context? = null
    private val BOTTOM_SHEET_TAG: String = "ACCOUNT_BOTTOM_SHEET_CONTACT_US"
    private lateinit var preferences: Preferences
    private lateinit var presenter: ProfilePresenter
    private lateinit var mainTabActivity: MainTabActivity
    override fun onAttach(context: Context) {
        super.onAttach(context)
        preferences = Preferences(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = ProfilePresenter(this)
        mainTabActivity = activity as MainTabActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMenu(getAccountMenuList(view.context))
        btn_edit_profile?.setOnClickListener {
            startActivity<ProfileEditActivity>()
        }
        btn_logout?.setOnClickListener {
            showConfirmationDialog()
        }
    }

    private fun logout(){
        preferences.userLoggedOut()
        activity?.finishAffinity()
        startActivity<MainActivity>()
    }

    override fun onResume() {
        super.onResume()
        loadData()
    }

    private fun initMenu(list: List<AccountMenuData>?) {
        val adapter = AccountMenuAdapter {
            when (it) {
                1 -> {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(Constants.Common.pingUrl)
                    startActivity(intent)
                }
                2 -> {
                    showContactBottomSheet()
                }
            }
        }
        adapter.listMenu = list
        rv_menu_profile?.adapter = adapter
    }

    private fun showContactBottomSheet() {
        val bottomSheet = mContext?.let { getCallerList(it) }?.let { ContactUsBottomSheet(it) }
        bottomSheet?.show(childFragmentManager, BOTTOM_SHEET_TAG)
    }

    private fun loadData() {
        showLoading()
        presenter.loadData()
    }

    override fun profileResponse(response: ProfileDisplayData) {
        hideLoading()
        setView(response)
    }

    private fun setView(data: ProfileDisplayData?) {
        tv_email_profile?.text = data?.email
        tv_nama_profile?.text = data?.username
        iv_user_profile?.let {
            Glide.with(applicationContext()).load(data?.photoUrl).placeholder(R.color.color_primary)
                .into(it)
        }
    }

    override fun showLoading(tag: String?) {
        content_profile?.apply {
            visibility = View.INVISIBLE
        }
        shimmer_profile?.apply {
            startShimmer()
            visible()
        }
    }

    override fun hideLoading(tag: String?) {
        shimmer_profile?.apply {
            stopShimmer()
            gone()
        }
        content_profile?.apply {
            visibility = View.VISIBLE
        }
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        mainTabActivity.showErrorCustomDialog(title, message)
    }

    fun showConfirmationDialog(){
        mainTabActivity.showConfirmationCustomDialog(
            "Logout",
            "Apakah anda yakin akan keluar?",
            object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    dialog.dismiss()
                    logout()
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }

            }
        )
    }


}

package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.digitasi.DigitasiJalan
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.digitasi.DigitasiJalanContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.digitasi.DigitasiJalanPresenter
import kotlinx.android.synthetic.main.activity_map_picker.*
import org.jetbrains.anko.toast

class MapPickerActivity : BaseActivity(), DigitasiJalanContract.View {
    var marker: Marker? = null
    var locationPermission = false
    var map: GoogleMap? = null
    var markerPosition: LatLng? = null
    var markerAddress: String = ""
    val returnedIntent = Intent()
    var currentLatLng: LatLng? = null
    var firstLoad = true
    private var isThereNearestRoad = false
    private val presenter = DigitasiJalanPresenter(this)
    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        googleMap.setOnMyLocationButtonClickListener {
            getDeviceLocation()
            true
        }

        getLocationPermission()
        try {
            getDeviceLocation(true)
            map?.isMyLocationEnabled = true

        } catch (e: SecurityException) {

        }
//        showLoading()
//        presenter.loadData()
    }


    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                GlobalClass.context,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermission = true
        } else {
            let {
                ActivityCompat.requestPermissions(
                    it, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    Constants.Permissions.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                )
            }
        }
    }

    private fun getDeviceLocation(isFirstLoad: Boolean = false) {
        val mLocation = let { LocationServices.getFusedLocationProviderClient(it) }
        try {
            if (locationPermission) {
                firstLoad = isFirstLoad
                removeMarker(marker)
                val location = mLocation?.lastLocation
                location?.addOnCompleteListener {
                    val currentLocation = it.result
                    val position = LatLng(currentLocation.latitude, currentLocation.longitude)
                    currentLatLng = position
                    currentLocation?.latitude?.let { it1 -> LatLng(it1, currentLocation.longitude) }
                    moveCamera(position)
                    marker?.position = position
                    setAddress(position)
                    marker = map?.addMarker(
                        MarkerOptions().position(position).icon(
                            BitmapDescriptorFactory.defaultMarker(
                                BitmapDescriptorFactory.HUE_BLUE
                            )
                        ).draggable(false)
                    )

//                    map?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
//                        override fun onMarkerDragStart(p0: Marker?) {
//                            Log.d("marker", "dragstart")
//                        }
//
//                        override fun onMarkerDrag(p0: Marker?) {
//                            Log.d("marker", "onDrag")
//                        }
//
//                        override fun onMarkerDragEnd(p0: Marker?) {
//                            p0?.position?.let { it1 -> setAddress(it1) }
//                        }
//                    })

                }
//                showLoading()
                presenter.loadData()
            }

        } catch (e: SecurityException) {

        }
    }

    fun removeMarker(m: Marker?) {
        m?.remove()
    }

    private fun setAddress(position: LatLng) {
        markerPosition = position
        moveCamera(position)
        val geocoder = Geocoder(this@MapPickerActivity)
        val addresses = geocoder.getFromLocation(position.latitude, position.longitude, 1)
        Logger.addLogAdapter(AndroidLogAdapter())
        Logger.d(addresses)
        val address = addresses?.get(0)?.getAddressLine(0)
        markerAddress = address.toString()
        til_address_picker?.editText?.setText(address)
    }

    private fun moveCamera(latLng: LatLng, zoom: Float? = 18f) {
        map?.animateCamera(zoom?.let { CameraUpdateFactory.newLatLngZoom(latLng, it) })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_picker)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(callback)
        card_information_image_picker?.setOnClickListener {
            showInformation()
        }
        btn_simpan_picker?.setOnClickListener {
            returnedIntent.apply {
                putExtra(Constants.Tags.latitude, markerPosition?.latitude)
                putExtra(Constants.Tags.longitude, markerPosition?.longitude)
                putExtra(Constants.Tags.address, markerAddress)
            }
            if (!returnedIntent.getStringExtra(Constants.Query.id_digitasi).isNullOrEmpty()) {
                setResult(Activity.RESULT_OK, returnedIntent)
                finish()
            } else {
                showErrorCustomDialog(
                    "Validasi",
                    "Jalan yang ingin anda adukan belum terdaftar pada sistem"
                )
            }
        }
    }

    private fun showInformation() {
        showInformationCustomDialog(
            "Informasi",
            "Aplikasi akan secara otomatis mendeteksi jalan yang akan anda adukan. \n\n" +
                    "Pengaduan akan langsung tertuju kepada pengelola jalan yang bersangkutan"
        )
    }

    override fun loadResponse(response: List<DigitasiJalan>) {
//        hideLoading()
//        showInformation()
        setPolyline(response)
    }

    private fun setPolyline(response: List<DigitasiJalan>) {
        val polylines = mutableListOf<Polyline>()
        val nearestPolylineResult = hashMapOf<String, Any?>()
        val booleanArray = mutableListOf<Boolean>()
        for (i in response) {
            val koordinatDigitasi = mutableListOf<LatLng>()
            for (koordinat in i.koordinatDigitasi!!) {
                val latitude = koordinat.latitude
                val longitude = koordinat.longitude
                val k = LatLng(latitude, longitude)
                koordinatDigitasi.add(k)
            }
            val polyline = map?.addPolyline(
                PolylineOptions()
                    .clickable(true)
                    .color(R.color.color_primary)
                    .zIndex(1f)
                    .addAll(
                        koordinatDigitasi
                    )
            )
            polyline?.let { polylines.add(it) }
            val index = polylines.indexOf(polyline)
            val item = response[index]

            val isNearestPolyline =
                PolyUtil.isLocationOnPath(
                    currentLatLng,
                    koordinatDigitasi,
                    true,
                    Constants.Maps.DISTANCE_TOLERATE
                )
            booleanArray.add(isNearestPolyline)
            if (isNearestPolyline) {
                selectPolyline(polyline, item)
                break
            }
            Logger.d(
                "${
                    PolyUtil.isLocationOnPath(
                        currentLatLng,
                        koordinatDigitasi,
                        true,
                        Constants.Maps.DISTANCE_TOLERATE
                    )
                } - ${item.ruasJalan}"
            )

            Logger.d(
                "${
                    PolyUtil.locationIndexOnEdgeOrPath(
                        currentLatLng,
                        koordinatDigitasi,
                        false,
                        true,
                        Constants.Maps.DISTANCE_TOLERATE
                    )
                } - ${item.ruasJalan}"
            )


        }

        if (!booleanArray.contains(true)) {
            showNoNearestRoadDialog()
        } else {
            if (firstLoad)
                showInformation()
        }
//        map?.setOnPolylineClickListener {
//            val index = polylines.indexOf(it)
//            val item = response[index]
////            val latLng = item.koordinatDigitasi?.get(0)?.let { it1 -> item.koordinatDigitasi?.get(0)
////                .let { it2 -> it2?.longitude?.let { it3 -> LatLng(it1.latitude , it3) } } }
////            latLng?.let { it1 -> moveCamera(it1) }
//
//            it.apply {
//                color = Color.BLUE
//                zIndex = 2f
//            }
//            for (i in response.indices) {
//                if (i != index) {
//                    polylines[i].apply {
//                        color = R.color.color_primary
//                        zIndex = 1f
//                    }
//                }
//            }
//            returnedIntent.putExtra(Constants.Query.id_digitasi, item.id)
//            toast("Jalan ${item.namaJalan} Terpilih")
//        }
    }

    private fun showNoNearestRoadDialog() {
        showError(
            "Validasi",
            "Jalan yang ingin anda adukan belum terdaftar pada sistem"
        )
    }

    private fun selectPolyline(polyline: Polyline?, item: DigitasiJalan) {
        polyline?.apply {
            color = Color.BLUE
            zIndex = 2f
        }
        returnedIntent.putExtra(Constants.Query.id_digitasi, item.id)
        toast("Jalan ${item.namaJalan} Terpilih")
    }

    override fun showLoading(tag: String?) {
        showProgressBar()
    }

    override fun hideLoading(tag: String?) {
        hideProgressBar()
    }

    override fun showError(title: String, message: String) {
//        hideLoading()
        showErrorCustomDialog(title, message)
    }
}
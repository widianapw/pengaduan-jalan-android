package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.pengaduan

import android.os.Bundle
import coil.load
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Utilities
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.PengaduanImage
import id.widianapw.sig_pengaduan_jalan_rusak.extension.gone
import id.widianapw.sig_pengaduan_jalan_rusak.extension.setStatusColor
import id.widianapw.sig_pengaduan_jalan_rusak.extension.switchDateFormatString
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanDetailContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanDetailPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.BaseActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.pengaduan.PengaduanImageAdapter
import kotlinx.android.synthetic.main.activity_pengaduan_detail.*

class PengaduanDetailActivity : BaseActivity(), PengaduanDetailContract.View {
    private val presenter = PengaduanDetailPresenter(this)
    private val imageTerperbaikiAdapter = PengaduanImageAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengaduan_detail)
        toolbar_pengaduan_detail?.setNavigationOnClickListener {
            onBackPressed()
        }
        val id = intent.getStringExtra(Constants.Tags.ARGS_ID)
        loadData(id)
        text_label_alamat?.gone()
    }

    private fun loadData(id: String?) {
        showLoading()
        id?.let { presenter.loadPengaduanDetail(it) }
    }

    override fun pengaduanDetailResponse(response: Pengaduan) {
        hideLoading()
        setUI(response)
    }

    private fun setUI(pengaduan: Pengaduan) {
        text_label_alamat?.visible()
        val tanggal =
            pengaduan.tanggal?.switchDateFormatString(
                Constants.Common.DATE_FORMAT_API,
                Constants.Common.DATE_FORMAT_READ
            )
        val statusArray =
            Utilities.getStringArray(resources.getStringArray(R.array.pengaduan_status))

        val images = pengaduan.images?.filter { it.type=="pengaduan" }?.toMutableList()
        if (images.isNullOrEmpty())
            images?.add(
                PengaduanImage(
                    "pengaduan",
                    pengaduan.pengaduanPhotoUrl ?: ""
                )
            )

        carousel_pengaduan_detail?.apply {
            setImageListener { position, imageView ->
                imageView.load(images?.get(position)?.image)
            }
            pageCount = images?.count() ?: 0
        }

        text_judul_pengaduan_detail?.text = pengaduan.judul
        text_desc_pengaduan_detail?.text = pengaduan.deskripsi
        text_tanggal_pengaduan_detail?.text = tanggal

        cv_status_pengaduan_detail?.setStatusColor(pengaduan.status)

        text_status_pengaduan_detail?.text = statusArray[pengaduan.status.toInt()].title

        text_pengelola_pengaduan_detail?.text =
            getString(R.string.terdisposisi_ke, pengaduan.pengelolaJalan)
        text_alamat_detail?.text = pengaduan.alamat

        if (pengaduan.status == "2") {
            layout_terperbaiki?.visible()
            rv_image_terperbaiki?.adapter = imageTerperbaikiAdapter
            pengaduan.images?.filter { it.type == "terperbaiki" }?.let {
                imageTerperbaikiAdapter.setData(
                    it
                )
            }
        }
    }

    override fun showLoading(tag: String?) {
        content_page_pengaduan_detail?.apply {
            gone()
        }
        shimmer_pengaduan_detail?.apply {
            startShimmer()
            visible()
        }
    }

    override fun hideLoading(tag: String?) {
        shimmer_pengaduan_detail?.apply {
            gone()
            stopShimmer()
        }
        content_page_pengaduan_detail?.apply {
            visible()
        }
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        showErrorCustomDialog(title, message)
    }
}
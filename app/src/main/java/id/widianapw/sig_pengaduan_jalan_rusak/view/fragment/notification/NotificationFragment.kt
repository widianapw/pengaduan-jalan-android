package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.notification

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification.Notification
import id.widianapw.sig_pengaduan_jalan_rusak.extension.gone
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.notification.NotificationContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.notification.NotificationPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.notification.NotificationAdapter
import kotlinx.android.synthetic.main.fragment_notification.*

class NotificationFragment : Fragment(), NotificationContract.View {
    var mContext: Context? = null
    private lateinit var mainTabActivity: MainTabActivity
    private lateinit var presenter: NotificationPresenter
    val mAdapter = NotificationAdapter()
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainTabActivity = activity as MainTabActivity
        presenter = NotificationPresenter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
        rv_notification.apply {
            layoutManager = LinearLayoutManager(mContext)
            adapter = mAdapter
        }

    }

    fun loadData() {
        showLoading()
        presenter.loadData()

    }

    override fun notificationResponse(response: List<Notification>) {
        mAdapter.setData(response)
        hideLoading()
    }

    override fun showLoading(tag: String?) {
        shimmer_notification?.apply {
            startShimmer()
            visible()
        }
    }

    override fun hideLoading(tag: String?) {
        shimmer_notification?.apply {
            stopShimmer()
            gone()
        }
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        mainTabActivity.showErrorCustomDialog(title, message)
    }

}
package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.home

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.gone
import id.widianapw.sig_pengaduan_jalan_rusak.extension.hideKeyboard
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.pengaduan.PengaduanPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.pengaduan.PengaduanDetailActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.pengaduan.PengaduanAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.support.v4.startActivity

class HomeFragment : Fragment(), PengaduanContract.View {
    private lateinit var mainTabActivity: MainTabActivity
    private lateinit var mContext: Context
    val presenter = PengaduanPresenter(this)
    var mAdapter: PengaduanAdapter? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainTabActivity = activity as MainTabActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = PengaduanAdapter {
            startActivity<PengaduanDetailActivity>(
                Constants.Tags.ARGS_ID to it
            )
        }

        et_search_home?.setOnKeyListener { v, keyCode, event ->
            val find = (v as EditText).text.toString().trim()
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                if (find.isNotBlank()) {
                    showLoading()
                    presenter.loadDataSearch(find)
                } else {
                    loadData()
                }
                hideKeyboard()
            }
            return@setOnKeyListener false
        }

        rv_home?.apply {
            layoutManager = LinearLayoutManager(mContext)
            adapter = mAdapter
        }
        sr_home?.setOnRefreshListener {
            loadData()
        }
        loadData()
    }

    private fun loadData() {
        showLoading()
        presenter.loadData()
    }

    override fun pengaduanResponse(response: List<Pengaduan>) {
        hideLoading()
        sr_home?.isRefreshing = false
        mAdapter?.setData(response)
    }

    override fun showLoading(tag: String?) {
        shimmer_home?.apply {
            startShimmer()
            visible()
        }
    }

    override fun hideLoading(tag: String?) {
        shimmer_home?.apply {
            stopShimmer()
            gone()
        }
    }

    override fun showError(title: String, message: String) {
        sr_home?.isRefreshing = false
        hideLoading()
        mainTabActivity.showErrorCustomDialog(title, message)
    }

}

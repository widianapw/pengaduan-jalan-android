package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.notification

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.notification.Notification
import id.widianapw.sig_pengaduan_jalan_rusak.extension.switchDateFormatString
import kotlinx.android.synthetic.main.item_notifikasi.view.*

class NotificationAdapter : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    var notificationList: List<Notification>? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_notifikasi, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = notificationList?.get(position)
        holder.itemView.apply {
            val tanggal = item?.tanggal?.switchDateFormatString(
                Constants.Common.DATE_FORMAT_API,
                Constants.Common.DATE_FORMAT_READ
            )
            text_title_notification?.text = item?.judul
            text_message_notification?.text = item?.deskripsi
            text_date_notification?.text = tanggal
        }
    }

    override fun getItemCount(): Int {
        return notificationList?.size ?: 0
    }

    fun setData(data: List<Notification>) {
        notificationList = data
        notifyDataSetChanged()
    }
}
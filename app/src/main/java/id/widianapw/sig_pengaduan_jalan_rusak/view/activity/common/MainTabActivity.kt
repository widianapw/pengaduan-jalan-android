package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import id.widianapw.sig_pengaduan_jalan_rusak.MapsFragment
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.home.HomeFragment
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.notification.NotificationFragment
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.profile.ProfileFragment
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.report.ReportFragment
import kotlinx.android.synthetic.main.activity_main_tab.*
import org.jetbrains.anko.toast

class MainTabActivity : BaseActivity() {
    private lateinit var preferences: Preferences
    private val navigationListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.menu_home -> {
                if (getDisplayedFragment().javaClass.simpleName != "HomeFragment")
                    addFragment(HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_map -> {
                if (getDisplayedFragment().javaClass.simpleName != "MapsFragment")
                    addStackFragment(MapsFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_report -> {
                if (getDisplayedFragment().javaClass.simpleName != "ReportFragment")
                    addStackFragment(ReportFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_notification -> {
                if (getDisplayedFragment().javaClass.simpleName != "NotificationFragment")
                    addStackFragment(NotificationFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.menu_profile -> {
                if (getDisplayedFragment().javaClass.simpleName != "ProfileFragment")
                    addStackFragment(ProfileFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tab)
        preferences = Preferences(this)
        bottom_navigation_main.setOnNavigationItemSelectedListener(navigationListener)
        Log.d("fcm_token",FirebaseInstanceId.getInstance().token)
        if (savedInstanceState == null) {
            val fragment = HomeFragment()
            addFragment(fragment)
        }
    }

    private fun addFragment(fragment: Fragment) {
        if (supportFragmentManager.backStackEntryCount != 0)
            supportFragmentManager
                .popBackStack()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    private fun getDisplayedFragment(): Fragment {
        return supportFragmentManager.findFragmentById(R.id.fragment_content) ?: HomeFragment()
    }

    private fun addStackFragment(fragment: Fragment) {
        Log.d("displayedFragment", getDisplayedFragment().javaClass.simpleName)
        Log.d("selectedFragment", fragment.javaClass.simpleName)
        if (getDisplayedFragment().javaClass.simpleName != "HomeFragment") {

            supportFragmentManager
                .popBackStack()

            supportFragmentManager
                .beginTransaction()
                .remove(getDisplayedFragment())
                .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
        } else {

            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_content, fragment, fragment.javaClass.simpleName)
                .addToBackStack(null)
                .commit()
        }

    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        Log.d("COunt Back", count.toString() + "  " + bottom_navigation_main.selectedItemId)
        if (count != 0) {
            bottom_navigation_main.menu.findItem(R.id.menu_home).isChecked = true
            super.onBackPressed()
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }
            doubleBackToExitPressedOnce = true
            toast(getString(R.string.tap_back_button_twice))
            Handler().postDelayed({
                doubleBackToExitPressedOnce = false
            }, 2000)
        }
    }
}

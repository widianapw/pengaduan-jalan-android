package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.profile

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.account.CallerList
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.profile.AccountCallerAdapter
import kotlinx.android.synthetic.main.bottomsheetdialog_account_contactus.*
import org.jetbrains.anko.support.v4.toast

class ContactUsBottomSheet(val callerList: List<CallerList>) : BottomSheetDialogFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.CatalogBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bottomsheetdialog_account_contactus, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        contactusbs_img_close?.setOnClickListener {
            dismiss()
        }
    }

    private fun initRecycler() {
        val adapter = AccountCallerAdapter(callerList) {
            when (it) {
                1 -> {
                    intentCall(Constants.ContactUs.phone)
                }
                2 -> {
                    intentWhatsApp(Constants.ContactUs.phone)
                }
                3 -> {
                    intentTelegram(Constants.ContactUs.telegram)
                }
            }
        }
        contactusbs_rv_container?.adapter = adapter
    }

    private fun intentCall(phone: String) {
        val uri = Uri.parse("tel:$phone")
        val intent = Intent(Intent.ACTION_DIAL, uri)
        startActivity(intent)
    }

    private fun intentTelegram(phone: String) {
        val appName = "org.telegram.messenger"
        if (isAppAvailable(appName)) {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.parse("http://telegram.me/$phone")
            intent.setPackage(appName)
            intent.data = uri
            startActivity(intent)
        } else {
            toast("Telegram tidak terinstall pada device ini")
        }
    }

    private fun intentWhatsApp(phone: String) {
        val appName = "com.whatsapp"
        if (isAppAvailable(appName)) {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.parse("https://api.whatsapp.com/send?phone=$phone")
            intent.setPackage(appName)
            intent.data = uri
            startActivity(intent)
            Log.d("url=",uri.toString())
        } else {
            toast("Whatsapp tidak terinstall pada device ini")
        }
    }

    private fun isAppAvailable(appName: String): Boolean {
        val pm = context?.packageManager
        try {
            pm?.getPackageInfo(appName, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

    }
}
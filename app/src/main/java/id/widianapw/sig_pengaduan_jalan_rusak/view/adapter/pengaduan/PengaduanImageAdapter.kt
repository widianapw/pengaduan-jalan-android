package id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.pengaduan

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.PengaduanImage
import id.widianapw.sig_pengaduan_jalan_rusak.extension.clickWithDebounce
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.PhotoViewerActivity
import kotlinx.android.synthetic.main.item_image_terperbaiki.view.*

class PengaduanImageAdapter : RecyclerView.Adapter<PengaduanImageAdapter.ViewHolder>() {
    var list = mutableListOf<PengaduanImage>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_image_terperbaiki, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.itemView.apply {
            iv_image_terperbaiki_item?.load(item.image)
            cv_image_terperbaiki?.clickWithDebounce {
                val intent = Intent(context, PhotoViewerActivity::class.java)
                intent.putExtra("image", item.image)
                context.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    fun setData(setTo: List<PengaduanImage>) {
        list = setTo.toMutableList()
        notifyDataSetChanged()
    }


}
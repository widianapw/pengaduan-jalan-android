package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.profile

import android.net.Uri
import android.os.Bundle
import android.util.Log
import coil.load
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Validations
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.ImageRatioData
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.profile.ProfileDisplayData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.clickWithDebounce
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileEditContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfileEditPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.profile.ProfilePresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.BaseActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.common.ImagePickerDialogFragment
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.common.ImagePickerListener
import kotlinx.android.synthetic.main.activity_profile_edit.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.toast
import java.io.File

class ProfileEditActivity : BaseActivity(), ProfileEditContract.View, ProfileContract.View {
    private val presenter = ProfilePresenter(this)
    private val editPresenter = ProfileEditPresenter(this)
    private var profilePictFile: File? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)
        loadData()
        initTextWatcher()
        initView()
    }

    fun loadData(){
        showLoading()
        presenter.loadData()
    }

    private fun initView() {
        toolbar_profile_edit?.setNavigationOnClickListener {
            onBackPressed()
        }
        iv_profile_edit?.clickWithDebounce {

            openImageResources()
        }
        btn_update_profile_edit?.setOnClickListener {
            if (isValidData()) {
                val data = hashMapOf<String, Any?>()
                data[Constants.Query.username] = et_name_profile_edit?.editText?.text
                data[Constants.Query.phone] = et_phone_profile_edit?.editText?.text
                editPresenter.updateData(data)
                showLoading()
            }
        }

    }


    private fun openImageResources() {
        val imageDialog =
            ImagePickerDialogFragment(ImageRatioData(1, 1), object : ImagePickerListener {
                override fun onCropImage(dialog: BottomSheetDialogFragment, uri: Uri) {
                    showLoading()
                    dialog.dismiss()
                    iv_profile_edit?.load(uri)
                    uploadImage(File(uri.path))
                }
            })
        imageDialog.show(supportFragmentManager, "")
//        CropImage.activity()
//            .setGuidelines(CropImageView.Guidelines.ON)
//            .setAspectRatio(1, 1)
//            .start(this)
    }

    private fun initTextWatcher() {
        Validations.removeErrorStyle(et_name_profile_edit, et_phone_profile_edit)
    }

    private fun isValidData(): Boolean {
        val valid = mutableListOf<Boolean>()
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_MINIMUM,
                editText = et_name_profile_edit,
                minimum = 4
            )
        )
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_MINIMUM,
                editText = et_phone_profile_edit,
                minimum = 8
            )
        )
        return !valid.contains(false)
    }


    override fun profileResponse(response: ProfileDisplayData) {
        hideLoading()
        setUIData(response)
    }

    override fun updateProfileResponse(response: ProfileDisplayData) {
        hideLoading()
        toast("Data Berhasil Diubah!")
    }

    private fun setUIData(data: ProfileDisplayData) {
        et_name_profile_edit?.editText?.setText(data.username)
        et_phone_profile_edit?.editText?.setText(data.phone)
        iv_profile_edit?.let {
            Glide.with(applicationContext).load(data.photoUrl).placeholder(R.color.color_primary)
                .into(it)
        }
    }

    override fun showLoading(tag: String?) {
        showProgressBar()
    }

    override fun hideLoading(tag: String?) {
        hideProgressBar()
    }

    override fun showError(tittle: String, message: String) {
        hideLoading()
        showErrorCustomDialog(tittle, message)
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            val result = CropImage.getActivityResult(data)
//            if (resultCode == Activity.RESULT_OK) {
//                val resultUri = result.uri
//                profilePictFile = File(resultUri.path!!)
//                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, resultUri)
//                iv_profile_edit?.setImageBitmap(bitmap)
//                showLoading()
//                uploadImage(profilePictFile!!)
//            }
//        }
//    }

    private fun uploadImage(profilePictFile: File) {
        val requestBody =
            RequestBody.create(MediaType.parse("multipart/form-data"), profilePictFile)
        val part = MultipartBody.Part.createFormData(
            "image",
            profilePictFile.name,
            requestBody
        )
        Log.d("imageName", profilePictFile.name)
        editPresenter.updateProfileImage(part)
    }

}

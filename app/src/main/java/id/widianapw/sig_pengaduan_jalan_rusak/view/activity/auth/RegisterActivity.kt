package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.auth

import android.os.Bundle
import com.google.firebase.iid.FirebaseInstanceId
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences
import id.widianapw.sig_pengaduan_jalan_rusak.common.Validations
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth.AuthContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth.AuthPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.BaseActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.startActivity


class RegisterActivity : BaseActivity(), AuthContract.View {
    private val authPresenter = AuthPresenter(this)
    private lateinit var preferences: Preferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        preferences = Preferences(this)
        initTextWatcher()
        button_register.setOnClickListener {
            if (isAllValid()) {
                registerProcess()
            }
        }
    }

    private fun registerProcess() {
        showLoading()
        val data = HashMap<String, Any?>()
        data[Constants.Query.email] = edit_text_email_register?.editText?.text.toString()
        data[Constants.Query.password] = edit_text_password_register?.editText?.text.toString()
        data[Constants.Query.phone] = edit_text_phone_register?.editText?.text.toString()
        data[Constants.Query.username] = edit_text_nama_register?.editText?.text.toString()
        data[Constants.Query.fcm_token] = FirebaseInstanceId.getInstance().token
        authPresenter.register(data)
    }

    private fun initTextWatcher() {
        Validations.removeErrorStyle(
            edit_text_nama_register,
            edit_text_email_register,
            edit_text_phone_register,
            edit_text_password_register,
            edit_text_confirm_password_register
        )
    }

    private fun isAllValid(): Boolean {
        val valid = mutableListOf<Boolean>()
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_MINIMUM,
                editText = edit_text_nama_register,
                minimum = 4
            )
        )

        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_EMAIL,
                editText = edit_text_email_register
            )
        )

        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_PHONE,
                Constants.Validator.VALIDATION_MINIMUM,
                Constants.Validator.VALIDATION_EMPTY,
                editText = edit_text_phone_register,
                minimum = 8
            )
        )

        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_MINIMUM,
                editText = edit_text_password_register,
                minimum = 6
            )
        )

        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                editText = edit_text_confirm_password_register
            )
        )

        if (edit_text_confirm_password_register?.editText?.text.toString() != edit_text_password_register?.editText?.text.toString()) {
            edit_text_confirm_password_register?.apply {
                error = null
                isErrorEnabled = true
                error = getString(R.string.password_not_matched)
            }
            valid.add(false)
        }

        return !valid.contains(false)
    }


    override fun authResponse(response: AuthData) {
        hideLoading()
        preferences.userLoggedIn = true
        preferences.accessToken = response.accessToken
        preferences.firebaseToken = FirebaseInstanceId.getInstance().token
        hideLoading()
        startActivity<MainTabActivity>()
        finishAffinity()
    }

    override fun showLoading(tag: String?) {
        showProgressBar()
    }

    override fun hideLoading(tag: String?) {
        hideProgressBar()
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        showErrorCustomDialog(title, message)
    }
}

package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.google.firebase.iid.FirebaseInstanceId
import id.widianapw.sig_pengaduan_jalan_rusak.MainActivity
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences

class SplashActivity : BaseActivity() {
    lateinit var preferences: Preferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        preferences = Preferences(this)
        setFirebaseToken()
    }

    private fun permissionCheck() {
        if (!isAllPermissionGranted(splashListPermission)) {
            permission.askPermission(this, splashListPermission)
        } else {
            Handler().postDelayed({
                if (preferences.userLoggedIn)
                    startActivity(Intent(this, MainTabActivity::class.java))
                else
                    startActivity(Intent(this, MainActivity::class.java))
                finish()
            }, 3000L)
        }
    }

    private fun setFirebaseToken() {
        preferences.firebaseToken = FirebaseInstanceId.getInstance().token
        permissionCheck()
    }

    override fun onPermissionGranted() {
        super.onPermissionGranted()
        Handler().postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, 3000L)
    }

}

package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.report

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Utilities
import id.widianapw.sig_pengaduan_jalan_rusak.common.Validations
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.pengaduan.Pengaduan
import id.widianapw.sig_pengaduan_jalan_rusak.extension.clickWithDebounce
import id.widianapw.sig_pengaduan_jalan_rusak.extension.compressSingleImage
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.report.ReportContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.report.ReportPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MapPickerActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.SimpleDialogListener
import id.widianapw.sig_pengaduan_jalan_rusak.view.adapter.report.ReportImageAdapter
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.common.ImagePickerDialogFragment
import id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.common.ImagePickerListener
import kotlinx.android.synthetic.main.fragment_report.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class ReportFragment : Fragment(), ReportContract.View {
    val presenter = ReportPresenter(this)
    lateinit var mContext: Context
    lateinit var mainTabActivity: MainTabActivity
    var latitude: Double? = 0.0
    var longitude: Double? = 0.0
    var id_digitasi: String? = ""
    var fileImage: File? = null
    var imageAdapter = ReportImageAdapter()
    var cameraUri: Uri? = null
    val MAXIMUM_IMAGE = 3

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainTabActivity = activity as MainTabActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initTextWatcher()

        rv_report_image?.adapter = imageAdapter

        btn_pick_lokasi?.clickWithDebounce {
            if (mainTabActivity.isLocationPermissionValid()) {
                val i = Intent(mContext, MapPickerActivity::class.java)
                startActivityForResult(i, Constants.Tags.ARGS_LOCATION)
            }
        }

        btn_tambah_gambar_lapor?.setOnClickListener {
            if (imageAdapter.itemCount < MAXIMUM_IMAGE) {
                openCamera()
//                showImagePicker()
            } else {
                mainTabActivity.showErrorCustomDialog("Validasi", "Maksimal foto adalah tiga")
            }
//            openImageResource()
        }
        btn_lapor.setOnClickListener {
            if (isAllValid()) {
                showConfirmationDialog()
            }
        }

    }

    private fun showImagePicker() {
        val imageDialog = ImagePickerDialogFragment(null, object : ImagePickerListener {
            override fun onCropImage(dialog: BottomSheetDialogFragment, uri: Uri) {
                val file = File(uri.path)
                imageAdapter.addData(file)
                dialog.dismiss()
            }
        })
        imageDialog.show(childFragmentManager, "")

    }

    private fun initTextWatcher() {
        Validations.removeErrorStyle(til_judul_lapor, til_deskripsi_lapor, til_lokasi_lapor)
    }

    private fun showConfirmationDialog() {
        mainTabActivity.showConfirmationCustomDialog(
            "Pengaduan",
            "Apakah anda yakin akan melakukan pengaduan ini?",
            object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    dialog.dismiss()
                    laporProcess()
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }

            }
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.Tags.ARGS_LOCATION -> {
                    latitude = data?.getDoubleExtra(Constants.Tags.latitude, 0.0)
                    longitude = data?.getDoubleExtra(Constants.Tags.longitude, 0.0)
                    id_digitasi = data?.getStringExtra(Constants.Query.id_digitasi)
                    val address = data?.getStringExtra(Constants.Tags.address)
                    til_lokasi_lapor?.editText?.setText(address)

                }
                Constants.Image.CAMERA_REQUEST_CODE -> {
                    cameraUri?.let {
                        cropImage(it)
                    }
                }
                CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                    val result = CropImage.getActivityResult(data)
                    result?.uri?.path?.let {
//                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                            val exif = ExifInterface(context?.contentResolver?.openInputStream(uri))
//                            val latLng = FloatArray(2)
//                            val hasLatLng = exif.getLatLong(latLng)
//                            Logger.d(latLng)
//                            Logger.d(hasLatLng)
//                        }
                        if (resultCode == Activity.RESULT_OK) {
                            val compressedFile = File(it).compressSingleImage()
                            val uri = Uri.fromFile(compressedFile)
                            setImageList(File(uri.path))
                        }
                    }
                }
            }

        }


    }

    private fun setImageList(fileImage: File) {
        imageAdapter.addData(fileImage)
    }

    private fun setImageView(bitmap: Bitmap?) {
//        iv_report?.visible()
//        btn_tambah_gambar_lapor?.gone()
//        btn_hapus_gambar_lapor?.visible()
//        iv_report?.setImageBitmap(bitmap)
    }

    private fun openImageResource() {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setAspectRatio(1, 1)
            .start(mContext, this)
    }

    private fun laporProcess() {
        val data = HashMap<String, Any?>()
        data[Constants.Query.judul] = til_judul_lapor?.editText?.text.toString()
        data[Constants.Query.deskripsi] = til_deskripsi_lapor?.editText?.text.toString()
        data[Constants.Query.latitude] = latitude
        data[Constants.Query.longitude] = longitude
        data[Constants.Query.alamat] = til_lokasi_lapor?.editText?.text.toString()
        data[Constants.Query.id_digitasi] = id_digitasi
        showLoading()
        presenter.lapor(data)
    }

    override fun laporResponse(response: Pengaduan) {
//        uploadImage(fileImage!!, response.id)
        uploadMultipleImage(imageAdapter.getData(), response.id)
    }

    private fun uploadMultipleImage(imageList: List<File>, id: String) {
//        imageList.forEach {
//
//        }
//        val requestBody =
//            RequestBody.create(MediaType.parse("multipart/form-data"), imageList)
//        val part = MultipartBody.Part.createFormData(
//            "image[]",
//            image.name,
//            requestBody
//        )
//        MultipartBody.Pa
//        Log.d("imageName", image.name)
//        presenter.uploadImagePengaduan(part, id)
//        val imagePart = MultipartBody.Part[imageList.size]
        val parts = ArrayList<MultipartBody.Part>()
        imageList.forEachIndexed { i, v ->
            val requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), v)
            val part = MultipartBody.Part.createFormData("image[$i]", v.name, requestBody)
            parts.add(part)
        }
        presenter.uploadMultiplaeImagePengaduan(parts, id)
    }

    private fun uploadImage(image: File, id: String) {
        val requestBody =
            RequestBody.create(MediaType.parse("multipart/form-data"), image)
        val part = MultipartBody.Part.createFormData(
            "image",
            image.name,
            requestBody
        )
        Log.d("imageName", image.name)
        presenter.uploadImagePengaduan(part, id)
    }

    override fun uploadImageResponse(response: Pengaduan) {
        hideLoading()
        mainTabActivity.showSuccessCustomDialog(
            "Berhasil!",
            mContext.getString(R.string.message_success_add_pengaduan),
            object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    dialog.dismiss()
                    mainTabActivity.onBackPressed()
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }
            }
        )
    }

    override fun showLoading(tag: String?) {
        mainTabActivity.showProgressBar()
    }

    override fun hideLoading(tag: String?) {
        mainTabActivity.hideProgressBar()
    }

    override fun showError(title: String, message: String) {
        mainTabActivity.showErrorCustomDialog(title, message)
    }

    private fun isAllValid(): Boolean {
        val valid = mutableListOf<Boolean>()
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                editText = til_judul_lapor
            )
        )
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                editText = til_deskripsi_lapor
            )
        )
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                editText = til_lokasi_lapor
            )
        )
        val isImageValid = imageAdapter.getData().isNotEmpty()
        valid.add(isImageValid)
        if (!isImageValid) {
            mainTabActivity.showErrorCustomDialog(
                "Validasi",
                "Wajib melampirkan foto dalam membuat pengaduan"
            )
        }
        return !valid.contains(false)
    }


    ///////// region open camera without picker

    private fun openCamera() {
        //take picture from camera and then save the file
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        //create file
        val file = Utilities.getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)

        //get the uri from file
        cameraUri = Uri.fromFile(file)
        with(Intent(MediaStore.ACTION_IMAGE_CAPTURE)) {
            putExtra(MediaStore.EXTRA_OUTPUT, cameraUri)
            startActivityForResult(this, Constants.Image.CAMERA_REQUEST_CODE)
        }
    }

    private fun cropImage(uri: Uri) {
        //crop image with camera uri (on open camera function)
        context?.let {
            CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .start(it, this)
        }
    }


}
package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.load
import id.widianapw.sig_pengaduan_jalan_rusak.R
import kotlinx.android.synthetic.main.activity_photo_viewer.*

class PhotoViewerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_viewer)
        val image = intent.getStringExtra("image")
        iv_photo_viewer?.load(image)
    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.auth

import android.os.Bundle
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Preferences
import id.widianapw.sig_pengaduan_jalan_rusak.common.Validations
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth.AuthContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.auth.AuthPresenter
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.BaseActivity
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.MainTabActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity


class LoginActivity : BaseActivity(), AuthContract.View {
    private val authPresenter = AuthPresenter(this)
    private lateinit var preferences: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        preferences = Preferences(this)
        initTextWatcher()
        button_login?.setOnClickListener {
            if (isValidInput()) {
                loginProcess()
            }
        }
        button_regiter_login.setOnClickListener {
            startActivity<RegisterActivity>()
        }
    }

    private fun initTextWatcher() {
        Validations.removeErrorStyle(edit_text_email_login, edit_text_password_login)
    }


    private fun isValidInput(): Boolean {
        val valid = mutableListOf<Boolean>()
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY,
                Constants.Validator.VALIDATION_EMAIL,
                editText = edit_text_email_login
            )
        )
        valid.add(
            Validations.isTextValid(
                Constants.Validator.VALIDATION_EMPTY, Constants.Validator.VALIDATION_MINIMUM,
                editText = edit_text_password_login,
                minimum = 6
            )
        )
        return !valid.contains(false)
    }


    private fun loginProcess() {
        showLoading()
        Log.d("Login FIREBASE", preferences.firebaseToken)
        val data = HashMap<String, Any?>()
        data[Constants.Query.email] = edit_text_email_login?.editText?.text.toString()
        data[Constants.Query.password] = edit_text_password_login?.editText?.text.toString()
        data[Constants.Query.fcm_token] = FirebaseInstanceId.getInstance().token
        authPresenter.login(data)
    }

    override fun authResponse(response: AuthData) {
        hideLoading()
        preferences.userLoggedIn = true
        preferences.accessToken = response.accessToken
        preferences.firebaseToken = FirebaseInstanceId.getInstance().token
        hideLoading()
        startActivity<MainTabActivity>()
        finishAffinity()
    }

    override fun showLoading(tag: String?) {
        showProgressBar()
    }

    override fun hideLoading(tag: String?) {
        hideProgressBar()
    }

    override fun showError(title: String, message: String) {
        hideLoading()
        showErrorCustomDialog(title, message)
    }
}

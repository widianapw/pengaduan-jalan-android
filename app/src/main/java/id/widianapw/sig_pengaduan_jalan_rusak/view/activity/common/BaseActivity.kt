package id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.libraries.places.api.Places
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Permission
import id.widianapw.sig_pengaduan_jalan_rusak.common.PermissionContract
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible

interface SimpleDialogListener {
    fun OnOkListener(dialog: Dialog)
    fun OnCancelListener(dialog: Dialog)
}

open class BaseActivity : AppCompatActivity(), PermissionContract.View {
    private lateinit var dialog: Dialog
    open var splashListPermission = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    open var locationListPermission = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    open var mapListPermission = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )
    lateinit var permission: PermissionContract.Presenter
    override var isPermissionGranted: Boolean = false
    var isDialogShow = false
    private var warningDialog: Dialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
        Places.initialize(applicationContext, getString(R.string.google_maps_key))
        permission = Permission(this, this)
    }

    fun showProgressBar() {
//        pb_main?.visible()

        dialog = Dialog(this)
        val view = LayoutInflater.from(this).inflate(R.layout.layout_progress_dialog, null)
        dialog.run {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawableResource(R.color.transparent)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            setContentView(view)
            show()
        }
    }

    fun hideProgressBar() {
//        pb_main?.gone()
        if (this::dialog.isInitialized && dialog.isShowing) dialog.dismiss() else {
            dialog.dismiss()
        }
    }

    fun isAllPermissionGranted(list: Array<String>): Boolean {
        val arrayPermission = permission.checkPermission(list)
        isPermissionGranted = arrayPermission.isEmpty()
        return isPermissionGranted
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            showCustomDialog(false)
            onPermissionGranted()
        } else {
            Log.i("permission", "isdenied")
//            showCustomDialog(true)
        }
    }

    private fun showCustomDialog(isShow: Boolean) {
        if (isShow) {
            Log.i("permission", "isdenied3")
            if (!isDialogShow) {
                Log.i("permission", "isdenied4")
                isDialogShow = true
                warningDialog = buildAlertDialog(
                    getString(R.string.permission_denied_title),
                    getString(R.string.permission_denied_messages)
                )
                    .setCancelable(false)
                    .setPositiveButton("OK") { dialog, _ ->
                        with(Intent()) {
                            finish()
                            this.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                            val uri = Uri.fromParts("package", packageName, null)
                            this.data = uri
                            startActivity(this)
                        }
                    }
                    .show()
            }
        } else {
            isDialogShow = false
            warningDialog?.dismiss()
        }
    }

    override fun onPermissionGranted() {
        Log.d("Permission", "Permission Granted")
    }

    fun showErrorCustomDialog(tittle: String, message: String) {
        showDialog(R.drawable.warning,
            tittle,
            message,
            yesButton = getString(R.string.oke),
            noButton = null,
            context = this,
            listener = object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    dialog.dismiss()
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }
            })

    }

    fun showInformationCustomDialog(tittle: String, message: String) {
        showDialog(R.drawable.information,
            tittle,
            message,
            yesButton = "Oke",
            noButton = null,
            context = this,
            listener = object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    dialog.dismiss()
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }
            }
        )
    }

    private fun showDialog(
        logoResource: Int,
        title: String?,
        message: String?,
        yesButton: String? = "Oke",
        noButton: String? = "Batalkan",
        context: Context,
        listener: SimpleDialogListener
    ) {
        val customView = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
        val mTitle = customView.findViewById<TextView>(R.id.title)
        val mMessage = customView.findViewById<TextView>(R.id.message)
        val mYesButton = customView.findViewById<Button>(R.id.yesButton)
        val mNoButton = customView.findViewById<Button>(R.id.notButton)
        val mImage = customView.findViewById<ImageView>(R.id.img)

        val dialog = MaterialAlertDialogBuilder(this).run {
            setView(customView)
            setCancelable(false)
            //content
            mImage?.setImageResource(logoResource)
            title?.let {
                mTitle?.visible()
                mTitle?.text = it
            }
            message?.let {
                mMessage?.visible()
                mMessage?.text = it
            }
            yesButton?.let {
                mYesButton?.text = it
            }

            noButton?.let {
                mNoButton?.visible()
                mNoButton?.text = it
            }


            create()
        }

        mYesButton?.setOnClickListener {
            listener.OnOkListener(dialog)
        }

        mNoButton?.setOnClickListener {
            listener.OnCancelListener(dialog)
        }

        dialog.show()
    }

    fun showConfirmationCustomDialog(
        tittle: String,
        message: String,
        listener: SimpleDialogListener
    ) {
        showDialog(
            R.drawable.information,
            tittle,
            message,
            yesButton = "Oke",
            noButton = "Batal",
            context = this,
            listener = listener
        )
    }

    fun showSuccessCustomDialog(tittle: String, message: String, listener: SimpleDialogListener) {
        showDialog(
            R.drawable.ic_large_check,
            tittle,
            message,
            yesButton = "Oke",
            noButton = null,
            context = this,
            listener = listener
        )
    }

    fun isLocationPermissionValid(): Boolean {
        return if (isAllPermissionGranted(locationListPermission)) {
            val locationManager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                true
            } else {
                showAlertNoGPS()
                false
            }
        } else {
            false
        }
    }

    private fun showAlertNoGPS() {
        showConfirmationCustomDialog(
            "GPS tidak aktif",
            "Tolong hidupkan GPS agar dapat menggunakan fitur ini",
            object : SimpleDialogListener {
                override fun OnOkListener(dialog: Dialog) {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }

                override fun OnCancelListener(dialog: Dialog) {
                    dialog.dismiss()
                }

            })
    }

    private fun buildAlertDialog(title: String, message: String?): MaterialAlertDialogBuilder =
        MaterialAlertDialogBuilder(this).setTitle(title).setMessage(message)
}
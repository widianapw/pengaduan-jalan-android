package id.widianapw.sig_pengaduan_jalan_rusak.view.fragment.common

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import id.widianapw.sig_pengaduan_jalan_rusak.common.Utilities
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.ImageRatioData
import id.widianapw.sig_pengaduan_jalan_rusak.extension.clickWithDebounce
import id.widianapw.sig_pengaduan_jalan_rusak.extension.compressSingleImage
import kotlinx.android.synthetic.main.fragment_image_picker_dialog.*
import java.io.File

class ImagePickerDialogFragment(
    private val ratio: ImageRatioData?,
    private val listener: ImagePickerListener
) : BottomSheetDialogFragment() {

    var cameraUri: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_picker_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Logger.addLogAdapter(AndroidLogAdapter())
        camera_picker?.clickWithDebounce {
            openCamera()
        }

        btn_close_image_picker?.clickWithDebounce {
            dismiss()
        }

        gallery_picker?.clickWithDebounce {
            openGallery()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.Image.CAMERA_REQUEST_CODE -> {
                    cameraUri?.let {
                        cropImage(it)
                    }
                }
                CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                    val result = CropImage.getActivityResult(data)
                    result?.uri?.path?.let {
//                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                            val exif = ExifInterface(context?.contentResolver?.openInputStream(uri))
//                            val latLng = FloatArray(2)
//                            val hasLatLng = exif.getLatLong(latLng)
//                            Logger.d(latLng)
//                            Logger.d(hasLatLng)
//                        }
                        if (resultCode == Activity.RESULT_OK) {
                            val compressedFile = File(it).compressSingleImage()
                            val uri = Uri.fromFile(compressedFile)
                            listener.onCropImage(this, uri)
                        }
                    }
                }
            }

        }
    }
    //endregion

    //region local function
    private fun openCamera() {
        //take picture from camera and then save the file
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        //create file
        val file = Utilities.getOutputMediaFile(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)

        //get the uri from file
        cameraUri = Uri.fromFile(file)
        with(Intent(MediaStore.ACTION_IMAGE_CAPTURE)) {
            putExtra(MediaStore.EXTRA_OUTPUT, cameraUri)
            startActivityForResult(this, Constants.Image.CAMERA_REQUEST_CODE)
        }
    }

    private fun cropImage(uri: Uri) {
        //crop image with camera uri (on open camera function)
        context?.let {
            if (ratio != null) {
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(ratio.x, ratio.y).start(it, this)
            } else {
                CropImage.activity(uri).setGuidelines(CropImageView.Guidelines.ON).start(it, this)
            }
        }
    }

    private fun openGallery() {
        //open library with disable camera
        context?.let {
            if (ratio != null) {
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).disableCamera()
                    .setAspectRatio(ratio.x, ratio.y).start(
                        it, this
                    )
            } else {
                CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).disableCamera()
                    .start(
                        it, this
                    )
            }
        }
    }
    //endregion

}

interface ImagePickerListener {
    fun onCropImage(dialog: BottomSheetDialogFragment, uri: Uri)
}

package id.widianapw.sig_pengaduan_jalan_rusak.extension

import androidx.core.content.ContextCompat
import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass


fun Int.getColor(): Int {
    return ContextCompat.getColor(GlobalClass.context, this)
}
//fun Int.toStatus() : String {
//    return when(this){
//        Constants.Query.customer_listEnum[0] -> R.string.registered.getString()
//        Constants.Query.customer_listEnum[1] -> R.string.pending.getString()
//        Constants.Query.customer_listEnum[2] -> R.string.sold.getString()
//        Constants.Query.customer_listEnum[3] -> R.string.invoice_customer.getString()
//        Constants.Query.customer_listEnum[4] -> R.string.paperwork.getString()
//        Constants.Query.customer_listEnum[5] -> R.string.goods_delivered.getString()
//        else -> R.string.deal_closed.getString()
//    }
//}

//fun Int.toCareType(): String?{
//    return when(this){
//        Constants.Query.care_type[0] -> R.string.warranty_care.getString()
//        Constants.Query.care_type[1] -> R.string.after_care.getString()
//        else -> R.string.warranty_plus_after.getString()
//    }
//}
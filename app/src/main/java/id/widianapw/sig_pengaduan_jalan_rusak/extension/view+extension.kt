package id.widianapw.sig_pengaduan_jalan_rusak.extension

import android.os.SystemClock
import android.view.View
import com.google.android.material.card.MaterialCardView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants

fun View.clickWithDebounce(debounceTime: Long = 600L, action: (View) -> Unit) {

    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action(v)

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}

fun MaterialCardView.setStatusColor(status: String) {
    val color =
        when (status) {
            "0" -> R.color.quantum_yellow800.getColor()
            "1" -> R.color.color_primary.getColor()
            "2" -> R.color.quantum_lightgreen.getColor()
            "4" -> R.color.red_btn_bg_pressed_color.getColor()
            else -> R.color.quantum_lightgreen.getColor()
        }

    this.setCardBackgroundColor(color)
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.isVisible(): Boolean{
    return (this.visibility == View.VISIBLE)
}

fun View.isGone(): Boolean{
    return (this.visibility == View.GONE)
}

fun View.isInvisible(): Boolean{
    return (this.visibility == View.INVISIBLE)
}

fun View.setVisibility(tag: String){
    when(tag){
        Constants.View.visible -> this.visibility = View.VISIBLE
        Constants.View.invisible -> this.visibility = View.INVISIBLE
        Constants.View.gone -> this.visibility = View.GONE
    }
}
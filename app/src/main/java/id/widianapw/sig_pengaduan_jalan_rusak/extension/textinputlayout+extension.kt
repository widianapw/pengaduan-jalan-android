package id.widianapw.sig_pengaduan_jalan_rusak.extension

import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputLayout
import id.widianapw.sig_pengaduan_jalan_rusak.R
import java.util.regex.Pattern


fun TextInputLayout.isValidInput(
    validator: Pattern? = null,
    min: Int,
    max: Int,
    value: (String) -> Unit
): Boolean {
    return when {
        this.editText?.text.isNullOrEmpty() -> {
            value("")
            isErrorEnabled = true
            error = R.string.error_empty.getString()
            false
        }
        this.editText?.text?.length!! < min -> {
            value("")
            isErrorEnabled = true
            error = "${R.string.minimum.getString()} $min ${R.string.character.getString()}"
            false
        }
        this.editText?.text?.length!! > max -> {
            value("")
            isErrorEnabled = true
            error = "${R.string.maximum.getString()} $max ${R.string.character.getString()}"
            false
        }
        validator == null -> {
            return true
        }
        validator.matcher(editText?.text ?: "").find() -> {
            value(editText?.text.toString())
            error = null
            isErrorEnabled = false
            true
        }
        else -> {
            value("")
            isErrorEnabled = true
            error = "${R.string.wrong_format.getString()}"
            false
        }
    }
}

fun TextInputLayout.isValidInput(): Boolean {
    return !editText?.text.isNullOrEmpty()
}

fun TextInputLayout.removeErrorStyle() {
    this.editText?.doOnTextChanged { _, _, _, _ ->
        this.error = null
    }
}

fun TextInputLayout.isValidPassword(password: String, value: (String) -> Unit): Boolean {
    return when {
        editText?.text.isNullOrEmpty() -> {
            value("")
            isErrorEnabled = true
            error = R.string.error_empty.getString()
            false
        }
        password.equals(editText?.text.toString(), ignoreCase = false) -> {
            value(editText?.text.toString())
            error = null
            isErrorEnabled = false
            true
        }
        else -> {
            value("")
            isErrorEnabled = true
            error = R.string.password_not_matched.getString()
            false
        }
    }
}
//fun TextInputLayout.isValidEmail(emptyable: Boolean, value: (String) -> Unit) : Boolean {
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            if(emptyable){
//                value(editText?.text.toString())
//                error = null
//                isErrorEnabled = false
//                true
//            }else{
//                value("")
//                isErrorEnabled = true
//                error = R.string.error_empty.getString()
//                false
//            }
//        }
//        Constants.Common.EMAIL_PATTERN.matcher(editText?.text ?: "").find() -> {
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//        else -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.wrong_format_email.getString()
//            false
//        }
//    }
//}
//fun TextInputLayout.isValidEmail(emptyable: Boolean) : Boolean {
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            emptyable
//        }
//        Constants.Common.EMAIL_PATTERN.matcher(editText?.text ?: "").find() -> {
//            true
//        }
//        else -> {
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isAlphanumeric(emptyable: Boolean, value: (String) -> Unit) : Boolean {
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            if(emptyable){
//                value(editText?.text.toString())
//                error = null
//                isErrorEnabled = false
//                true
//            }else{
//                value("")
//                isErrorEnabled = true
//                error = R.string.error_empty.getString()
//                false
//            }
//        }
//        this.editText?.text?.length!! <4 -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.minimum_4.getString()
//            false
//        }
//        Constants.Common.ALPHANUMERIC.matcher(editText?.text ?: "").find() -> {
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//        else -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.not_alphanumeric.getString()
//            false
//        }
//    }
//}
//fun TextInputLayout.isAlphanumeric(emptyable: Boolean) : Boolean {
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            emptyable
//        }
//        Constants.Common.ALPHANUMERIC.matcher(editText?.text ?: "").find() -> {
//            true
//        }
//        else -> {
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isCharacter(value: (String) -> Unit): Boolean {
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.error_empty.getString()
//            false
//        }
//        Constants.Common.CHARACTERONLY.matcher(editText?.text ?: "").matches() -> {
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//        else -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.contain_special_char.getString()
//            false
//        }
//    }
//}
//fun TextInputLayout.isCharacter(): Boolean{
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            false
//        }
//        Constants.Common.CHARACTERONLY.matcher(editText?.text ?: "").matches() -> {
//            true
//        }
//        else -> {
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isAlphabet(minimum: Int, value: (String) -> Unit) : Boolean {
//    return if(this.editText?.text.isNullOrEmpty()){
//        value("")
//        isErrorEnabled = true
//        error = R.string.error_empty.getString()
//        false
//    }else{
//        if(Constants.Common.ALPHABETONLY.matcher(editText?.text ?: "").matches()){
//            if(this.editText?.text?.length!! < minimum){
//                value("")
//                isErrorEnabled = true
//                error = if(minimum==10) R.string.must_at_least_10.getString()
//                else R.string.must_at_least_4.getString()
//                false
//            }else{
//                value(editText?.text.toString())
//                error = null
//                isErrorEnabled = false
//                true
//            }
//        }else{
//            value("")
//            isErrorEnabled = true
//            error = R.string.contain_special_char.getString()
//            false
//        }
//    }
//}
//fun TextInputLayout.isAlphabet(minimum: Int) : Boolean {
//    return if(this.editText?.text.isNullOrEmpty()){
//        false
//    }else{
//        if(Constants.Common.ALPHABETONLY.matcher(editText?.text ?: "").matches()){
//            this.editText?.text?.length!! >= minimum
//        }else{
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isAlphabetNormal(minimum: Int,value: (String) -> Unit): Boolean{
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            error = null
//            isErrorEnabled = false
//            true
//        }
//        Constants.Common.ALPHABETONLY.matcher(editText?.text ?: "").matches() -> {
//            if((this.editText?.text?.length!! < 4) && (this.editText?.text?.length!! > 0)){
//                value("")
//                isErrorEnabled = true
//                error = if(minimum==10) R.string.must_at_least_10.getString()
//                else R.string.must_at_least_4.getString()
//                false
//            }else{
//                value(editText?.text.toString())
//                error = null
//                isErrorEnabled = false
//                true
//            }
//        }
//        else -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.contain_special_char.getString()
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isNotEmpty(value: (String) -> Unit) : Boolean {
//    return if (!editText?.text.isNullOrEmpty()) {
//        value(editText?.text.toString())
//        error = null
//        isErrorEnabled = false
//        true
//    } else {
//        value("")
//        isErrorEnabled = true
//        error = R.string.error_empty.getString()
//        false
//    }
//}
//
//fun TextInputLayout.isValidAddress(value: (String) -> Unit) : Boolean {
//    return when {
//        editText?.text?.length!! >=10 -> {
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//        editText?.text?.length!! > 0 -> {
//            value("")
//            isErrorEnabled = true
//            error = R.string.minimum_10.getString()
//            false
//        }
//        else ->{
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//    }
//}
//
//fun TextInputLayout.isValidAddress(): Boolean{
//    return when {
//        this.editText?.text.isNullOrEmpty() -> {
//            true
//        }
//        editText?.text?.length!! >=10 -> {
//            true
//        }
//        else -> {
//            false
//        }
//    }
//}
//
//fun TextInputLayout.isNotEmpty() : Boolean {
//    return !editText?.text.isNullOrEmpty()
//}
//
//fun TextInputLayout.isValidNumber(emptyable: Boolean, value: (String) -> Unit) : Boolean {
//    return when{
//        editText?.text.isNullOrEmpty() ->{
//            if(emptyable){
//                value(editText?.text.toString())
//                error = null
//                isErrorEnabled = false
//                true
//            }else{
//                value("")
//                isErrorEnabled = true
//                error = R.string.error_empty.getString()
//                false
//            }
//        }
////        (editText?.text?.length!! < 8) ->{
////            value("")
////            isErrorEnabled = true
////            error = R.string.must_at_least_8.getString()
////            false
////        }
////        (editText?.text?.length!! > 15) ->{
////            value("")
////            isErrorEnabled = true
////            error = R.string.maximum_digit_15.getString()
////            false
////        }
//        else ->{
//            value(editText?.text.toString())
//            error = null
//            isErrorEnabled = false
//            true
//        }
//    }
//}
//
//fun TextInputLayout.isValidNumber(emptyable: Boolean) : Boolean {
//    return when{
//        editText?.text.isNullOrEmpty() ->{
//            emptyable
//        }
//        (editText?.text?.length!! < 8) ->{
//            false
//        }
//        (editText?.text?.length!! > 15) ->{
//            false
//        }
//        else ->{
//            true
//        }
//    }
//}
package id.widianapw.sig_pengaduan_jalan_rusak.extension


import id.widianapw.sig_pengaduan_jalan_rusak.common.Constants
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T: Any> Observable<T>.doSubscribe(observer: Observer<in T>) {
    this.subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .retry(Constants.Common.retryTimes)
        .subscribe(observer)
}
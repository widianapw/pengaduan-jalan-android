package id.widianapw.sig_pengaduan_jalan_rusak.extension

import com.developers.imagezipper.ImageZipper
import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

fun Date.format(format: String): String {
    val simpleDateFormat = SimpleDateFormat(format, Locale.getDefault())
    return simpleDateFormat.format(this)
}

fun File.sizeInKb(): Long {
    return this.length().div(1024)
}

fun File.compressSingleImage(): File {
    var imageFile = this
    val quality = 20
    while (imageFile.length() / 1024 >= 1200) {
        try {
            imageFile = ImageZipper(GlobalClass.applicationContext())
                .setQuality(quality)
                .compressToFile(imageFile)
        } catch (e: Exception) {
            break
        }
    }
    return imageFile
}


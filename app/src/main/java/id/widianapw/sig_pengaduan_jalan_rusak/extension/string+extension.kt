package id.widianapw.sig_pengaduan_jalan_rusak.extension


import id.widianapw.sig_pengaduan_jalan_rusak.common.GlobalClass
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

fun Int.getString(string: String? = null): String {
    string?.let {
        return  GlobalClass.applicationContext().getString(this, string)
    }
    return GlobalClass.applicationContext().getString(this)
}

fun String.switchDateFormatString(fromFormat: String, toFormat: String): String{
    val parser = SimpleDateFormat(fromFormat)
    val formatter = SimpleDateFormat(toFormat, Locale("in"))
    return formatter.format(parser.parse(this)!!)
}

fun String.toPhoneFormat(): String{
    return if(this.startsWith("0")) this.replaceFirst("0","62")
    else if(this.startsWith("8")) "62$this"
    else this
}

//fun String.toTopic(): String{
//    return if(this == "en") Constants.Common.topicEnum[0] else Constants.Common.topicEnum[1]
//}

fun Long.toPrice(): String{
    val locale = Locale("in", "ID")
    val formatter = NumberFormat.getCurrencyInstance(locale)
    return formatter.format(this.toDouble())
}
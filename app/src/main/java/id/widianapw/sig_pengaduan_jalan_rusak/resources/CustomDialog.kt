package id.widianapw.sig_pengaduan_jalan_rusak.resources

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.extension.visible
import id.widianapw.sig_pengaduan_jalan_rusak.view.activity.common.SimpleDialogListener
import kotlinx.android.synthetic.main.custom_dialog.*

class CustomDialog(
    val logoResource: Int,
    val title: String?,
    val message: String?,
    val yesButton: String? = "Oke",
    val noButton: String? = "Batalkan",
    context: Context,
    val listener: SimpleDialogListener
) : Dialog(context) {
    init {
        val customView = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
        val mTitle = customView.findViewById<TextView>(R.id.title)
        val mMessage = customView.findViewById<TextView>(R.id.message)
        val mYesButton = customView.findViewById<Button>(R.id.yesButton)
        val mNoButton = customView.findViewById<Button>(R.id.notButton)
        val mImage = customView.findViewById<ImageView>(R.id.img)

        mImage?.setImageResource(logoResource)
        title?.let {
            mTitle?.visible()
            mTitle?.text = it
        }
        message?.let {
            mMessage?.visible()
            mMessage?.text = it
        }
        yesButton?.let {
            mYesButton?.text = it
        }

        noButton?.let {
            mNoButton?.visible()
            mNoButton?.text = it
        }

        mYesButton?.setOnClickListener {
            listener.OnOkListener(this)
        }

        mNoButton?.setOnClickListener {
            listener.OnCancelListener(this)
        }
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setContentView(customView)
        setCanceledOnTouchOutside(false)
        setCancelable(false)

    }
}
package id.widianapw.sig_pengaduan_jalan_rusak.common

import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputLayout
import id.widianapw.sig_pengaduan_jalan_rusak.R

object Validations {
    fun isTextValid(vararg types: Int, editText: TextInputLayout, minimum: Int = 0): Boolean {
        for (type in types) {
            when (type) {
                Constants.Validator.VALIDATION_EMPTY -> {
                    if (editText.editText?.text.toString().trim().isEmpty()) {
                        editText.apply {
                            error = null
                            isErrorEnabled = true
                            error = context.getString(R.string.error_empty)
                        }
                        return false
                    }
                }

                Constants.Validator.VALIDATION_EMAIL -> {
                    if (!Constants.Validator.EMAILPATTERN.matcher(
                            editText.editText?.text.toString().trim()
                        ).find()
                    ) {
                        editText.apply {
                            error = null
                            isErrorEnabled = true
                            error = context.getString(R.string.wrong_format)
                        }
                        return false
                    }
                }

                Constants.Validator.VALIDATION_PHONE -> {
                    if (!Constants.Validator.NUMBERONLY.matcher(
                            editText.editText?.text.toString()
                        ).find()
                    ) {
                        editText.apply {
                            error = null
                            isErrorEnabled = true
                            error = context.getString(R.string.wrong_format)
                        }
                        return false
                    }
                }

                Constants.Validator.VALIDATION_MINIMUM -> {
                    if (editText.editText?.text.toString().count() < minimum) {
                        editText.apply {
                            error = null
                            isErrorEnabled = true
                            error =
                                context.getString(R.string.minimum) + " $minimum " + context.getString(
                                    R.string.character
                                )
                        }
                        return false
                    }
                }
            }
        }
        return true
    }

    fun removeErrorStyle(vararg editText: TextInputLayout) {
        for (et in editText) {
            et.editText?.doOnTextChanged { _, _, _, _ ->
                et.apply {
                    error = null
                    isErrorEnabled = false
                }
            }
        }
    }
}
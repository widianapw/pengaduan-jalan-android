package id.widianapw.sig_pengaduan_jalan_rusak.common

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat

class Permission(val context: Context, val view: PermissionContract.View) :
    PermissionContract.Presenter, ActivityCompat.OnRequestPermissionsResultCallback {
    internal val MY_PERMISSION_REQUEST = 111

    override fun checkPermission(permissions: Array<out String>): Array<out String> {
        val list = arrayListOf<String>()
        permissions.forEach {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(
                        context,
                        it
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    list.add(it)
                }
            }
        }
        return list.toArray(arrayOf())
    }

    override fun askPermission(activity: Activity, permission: Array<out String>) =
        ActivityCompat.requestPermissions(activity, permission, MY_PERMISSION_REQUEST)

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        val list = arrayListOf<String>()
        permissions.forEach {
            if (grantResults.isNotEmpty()) {
                if (requestCode == MY_PERMISSION_REQUEST) {
                    view.isPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                } else {
                    list.add(it)
                }
            }
        }

        if (list.isNotEmpty()) view.isPermissionGranted = false
    }

    override fun isDeviceSupportCamera(): Boolean = context.packageManager.hasSystemFeature(
        PackageManager.FEATURE_CAMERA
    )
}

interface PermissionContract {
    interface View {
        var isPermissionGranted: Boolean
        fun onPermissionGranted()
    }

    interface Presenter {
        fun checkPermission(permissions: Array<out String>): Array<out String>
        fun askPermission(activity: Activity, permission: Array<out String>)
        fun isDeviceSupportCamera(): Boolean
    }
}
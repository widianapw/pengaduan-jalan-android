package id.widianapw.sig_pengaduan_jalan_rusak.common

import android.os.Environment
import android.provider.MediaStore
import id.widianapw.sig_pengaduan_jalan_rusak.data.handler.common.BaseHandler
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.common.APIError
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object Utilities {
    fun parseError(response: Response<*>): APIError {
        val converter: Converter<ResponseBody, APIError> = BaseHandler().getClient()
            .responseBodyConverter(APIError::class.java, arrayOfNulls<Annotation>(0))

        val error: APIError

        try {
            error = converter.convert(response.errorBody())!!
        } catch (e: IOException) {
            e.printStackTrace()
            return APIError()
        }
        return error
    }

    data class Validate(val message: String, val bool: Boolean)

//    fun emailValidate(email: String): Validate {
//        var msg = ""
//        var bl = false
//        if (email.isEmpty()) {
//            msg = "Email is Required"
//        }
//        else if (!Constants.EMAIL_PATTERN.matcher(email).find()) {
//            msg = "Email Format is Wrong"
//        } else {
//            bl = true
//        }
//        return Validate(msg, bl)
//    }

    fun usernameValidate(username: String): Validate {
        var msg = ""
        var bl = false
        when {
            username.isEmpty() -> msg = "Username is Required"
            username.length > 19 -> msg = "Maximum Username is 20 Char"
            else -> bl = true
        }
        return Validate(msg, bl)
    }

    data class ItemData(val id: Int, val title: String)

    fun getStringArray(resources: Array<String>): List<ItemData> {
        val listItem = arrayListOf<ItemData>()
        resources.forEach {
            val item = it.split("_")
            listItem += ItemData(item[0].toInt(), item[1])
        }
        return listItem
    }
//    fun passwordValidate(password: String): Validate {
//        var msg = ""
//        var bl = false
//        if (password.isEmpty())
//            msg = "Password is Required"
//        else if (password.length !in 8..20)
//            msg = "Password Minimum is 8 char / Maximum is 20 Char"
//        else if (!Constants.PASSWORD_PATTERN.matcher(password).find())
//            msg = "Password only allow Alphanumeric Char"
//        else
//            bl = true
//        return Validate(msg, bl)
//    }

//    fun confirmPasswordValidate(password: String, confirmPassword: String): Validate{
//        var msg=""
//        var bl=false
//        if (password == confirmPassword){
//            bl = true
//        }else{
//            msg = "Password not Matched"
//        }
//        return Validate(msg,bl)
//    }

    fun getOutputMediaFile(type: Int): File? {
        val mediaStorageDir = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
            "Camera"
        )
        // Create the storage directory if it does not exist
        mediaStorageDir.apply {
            if (!exists()) {
                if (!mkdirs()) {
                    return null
                }
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        return when (type) {
            MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE -> {
                File("${mediaStorageDir.path}${File.separator}IMG_$timeStamp.jpg")
            }
            MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO -> {
                File("${mediaStorageDir.path}${File.separator}VID_$timeStamp.mp4")
            }
            else -> null
        }
    }
}
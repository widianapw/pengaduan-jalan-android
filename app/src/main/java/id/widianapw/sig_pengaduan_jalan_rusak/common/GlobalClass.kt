package id.widianapw.sig_pengaduan_jalan_rusak.common

import android.app.Application
import android.content.Context
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger

class GlobalClass : Application(){
    init {
        instance = this
    }

    companion object{
        private var instance: GlobalClass? = null
        lateinit var context: Context
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        Logger.addLogAdapter(AndroidLogAdapter())
    }
}
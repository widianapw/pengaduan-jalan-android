package id.widianapw.sig_pengaduan_jalan_rusak.common

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import id.widianapw.sig_pengaduan_jalan_rusak.R
import id.widianapw.sig_pengaduan_jalan_rusak.data.model.auth.AuthData
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.FirebaseContract
import id.widianapw.sig_pengaduan_jalan_rusak.presenter.common.FirebasePresenter

class MyFirebaseService : FirebaseMessagingService(), FirebaseContract.View {
    val presenter = FirebasePresenter(this)

         lateinit var preferences: Preferences
    override fun onNewToken(p0: String) {
        preferences = Preferences(this)
        preferences.firebaseToken = p0
        Log.d("refreshed token: ", p0)
        val data = HashMap<String, Any?>()
        data[Constants.Query.fcm_token] = p0
        presenter.updateFirebaseToken(data)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("remote message: ", remoteMessage.from)
        if (remoteMessage.data.isNotEmpty()) {
            Log.d("Message Payload: ", "" + remoteMessage.data)
        }
        if (remoteMessage.notification != null) {
            val title = remoteMessage.notification?.title
            val message = remoteMessage.notification?.body
            Log.d("Notification: ", message)
            title?.let { message?.let { it1 -> sendNotification(it, it1) } }
        }
    }

    private fun sendNotification(title: String, message: String) {
        val channelId = "default_channel_id"
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_logo)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_logo))
            .setContentTitle(title)
            .setContentText(message)
            .setStyle(NotificationCompat.BigTextStyle().bigText(message))
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setAutoCancel(true)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0, notificationBuilder.build())
    }

    override fun updateFirebaseTokenResponse(response: AuthData) {
        preferences.firebaseToken = response.fcmToken
        Log.d("update success", response.fcmToken)
    }

    override fun showLoading(tag: String?) {
        TODO("Not yet implemented")
    }

    override fun hideLoading(tag: String?) {
        TODO("Not yet implemented")
    }

    override fun showError(title: String, message: String) {
        Log.e("ERROR", message)
    }

}
package id.widianapw.sig_pengaduan_jalan_rusak.common

import java.util.regex.Pattern

class Constants {

    class Key {
        companion object {
            const val google_maps_key = "AIzaSyByQtwjh8Bws-4rOmSYlFVmCbcygyAkDLoY"
        }
    }

    class View {
        companion object {
            const val visible = "visible"
            const val invisible = "invisible"
            const val gone = "gone"
        }
    }

    class Common {
        companion object {
            const val pingUrl = "https://final.widianapw.com/"

            const val baseUrl = "https://final.widianapw.com/api/"

            //            const val baseUrl = "https://c51ecf5cd3a0.ngrok.io/api/"
            const val aboutUs = pingUrl
            const val google_place_api = "AIzaSyBdxwn9ARvmzweQfs6NkT7bqkTXsNfCekg"
            const val retryTimes: Long = 0
            const val OTP_MAX_TIMER: Long = 60000
            val topicEnum = listOf<String>("android-dev-en", "android-dev-id")
            const val DATE_FORMAT_READ = "EEEE, dd MMM yyyy"
            const val DATE_FORMAT_API = "yyyy-MM-dd'T'HH:mm:ss"
        }
    }

    class Validator {
        companion object {
            val EMAILPATTERN =
                Pattern.compile("^[a-zA-Z0-9\\._\\-\\+]+@[a-zA-Z0-9_\\-]+\\.[a-zA-Z\\.]+[a-zA-Z]$")
            val ALPHABETONLY = Pattern.compile("^[ A-Za-z0-9.,\\n]+(?:[ -][KANYEA-Za-z0-9. ]+)*\$")
            val ALPHANUMERIC = Pattern.compile("^[a-zA-Z0-9]*\$")
            val CHARACTERONLY = Pattern.compile("([\\w\\W,\\?\\!\\.\"]+)")
            val NUMBERONLY = Pattern.compile("^[0-9]*\$")
            val VALIDATION_EMPTY = 0
            val VALIDATION_PHONE = 1
            val VALIDATION_NAME = 2
            val VALIDATION_EMAIL = 3
            val VALIDATION_MINIMUM = 4
            val VALIDATION_ADDRESS = 5
            val VALIDATION_VALUE_ZERO = 6
        }
    }

    class Query {
        companion object {
            const val email = "email"
            const val password = "password"
            const val phone = "phone"
            const val username = "username"
            const val fcm_token = "fcm_token"
            const val judul = "judul"
            const val deskripsi = "deskripsi"
            const val latitude = "latitude"
            const val longitude = "longitude"
            const val alamat = "alamat"
            const val id_digitasi = "id_digitasi_jalan"
        }
    }


    class Preference {
        companion object {
            const val refreshToken = "refresh_token"
            const val language = "language"
            const val accessToken = "access_token"
            const val isLogin = "isLogin"
            const val firebaseToken = "firebase_token"
        }
    }

    class ContactUs {
        companion object {
            const val phone = "+6282146456432"
            const val telegram = "Widianapw"
        }
    }

    class APICtrl {
        companion object {
            const val login = "login"
            const val register = "register"
            const val getProfile = "getProfile"
            const val updateProfile = "updateProfile"
            const val updateProfileImage = "updateProfileImage"

            const val getNotification = "notification/getNotification"
            const val getPengaduan = "pengaduan/getPengaduan"
            const val getPengaduanSearch = "pengaduan/getPengaduanSearch/{key}"
            const val getPengaduanTerverifikasi = "pengaduan/getPengaduanTerverifikasi"
            const val getPengaduanDetail = "pengaduan/getPengaduanDetail/{id}"
            const val postPengaduan = "pengaduan/postPengaduan"
            const val uploadImagePengaduan = "pengaduan/uploadImagePengaduan/{id}"

            const val getDigitasi = "digitasi/getDigitasi"

            const val updateFcmToken = "updateFcmToken"
        }
    }

    class Permissions {
        companion object {
            const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100
            const val AUTO_COMPLETE_REQUEST = 1
        }
    }

    class Tags {
        companion object {
            const val BOTTOM_SHEET_TAG: String = "BOTTOM_SHEET"
            const val ARGS_ID = "id"
            const val ARGS_LOCATION = 1
            const val latitude = "latitude"
            const val longitude = "longitude"
            const val address = "address"
            const val shimmer = "shimmer"
        }
    }

    object Image {
        const val CAMERA_REQUEST_CODE = 123
    }

    object Maps {
        const val DISTANCE_TOLERATE = 50.0
    }
}